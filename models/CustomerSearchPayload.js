export type CustomerSearchPayload = {
  CN_ID: number,
  query: string,
};
