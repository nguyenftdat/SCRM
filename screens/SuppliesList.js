import React, { useState, useEffect } from 'react';
import {
    Container,
    Content,
    Text,
    Card,
    Button,
    Item,
    Input,
    Toast
} from 'native-base';
import {
    View,
    Alert
} from 'react-native';
import CardGroupThietBi from '../components/CardGroupThietBi';
import { GroupThietBi, ThietBiRow } from '../models/GroupThietBi';
import ModalAddThietBi from '../components/ModalAddThietBi';
import ModalAddIndoorEquip from '../components/ModalAddIndoorEquip';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SuppliesList = ({ route, navigation }) => {
    const { schedule, type, status, groups_ } = route.params;
    // schedule.MaThueBao = '';
    let groups: Array<GroupThietBi> = groups_ || [{
        SO_HOPDONG: '',
        MaThueBao: schedule.MaThueBao || schedule.TB_MATHUEBAO,
        ListThietBi: []
    }];
    const [state, setState] = React.useState({
        contractNumber: null,
        groups
    });
    useEffect(() => {
        if ((schedule.MaThueBao == null || schedule.MaThueBao == '') && !schedule.TB_MATHUEBAO) {
            setState({...state, groups: []})
        }
    }, []);
    const modalAddThietBi = React.useRef(null);
    const showModalAddThietBi = (index: number) => () => {
        modalAddThietBi.current.showModal(index);
    };

    const addGroup = () => {
        // console.log(state.groups);
        if (!state.contractNumber) {
            Toast.show({
                text: 'Vui lòng nhân viên nhập số hợp đồng.',
                duration: 5000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (state.contractNumber.length < 6) {
            Toast.show({
                text: 'Vui lòng nhập số hợp đồng từ 6 kí tự trở lên.',
                duration: 5000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else {
            setState((previousState) => ({
                contractNumber: '',
                groups: [
                    ...previousState.groups,
                    {
                        MaThueBao: '',
                        SO_HOPDONG: state.contractNumber,
                        ListThietBi: [],
                    },
                ],
            }));
        }
    };
    const removeGroup = (index: number) => () => {
        let groups: Array<GroupThietBi> = [...state.groups];
        groups.splice(index, 1);
        setState({ groups });
    };

    const addItem = (index: number, item: ThietBiRow) => {
        let groups: Array<GroupThietBi> = [...state.groups];
        groups[index].ListThietBi.push(item);
        setState({ groups });
    };
    const removeItem = (groupIdx: number) => (index: number) => () => {
        let groups: Array<GroupThietBi> = [...state.groups];
        groups[groupIdx].ListThietBi.splice(index, 1);
        setState({ groups });
    };
    const onSaveData = () => {
        storeData(state.groups);
        // console.log(state.groups);
        if (state.groups.length === 0) {
            Alert.alert(
                'Thông báo',
                'Hiện tại bạn chưa nhập thiết bị bạn có chắc chắn bỏ qua không.',
                [
                    {
                        text: 'YES',
                        onPress: () =>
                            navigation.navigate('FinishScheduleModal', {
                                schedule: schedule,
                                thietBiSuDung: state.groups,
                                type: type
                            }),
                    },
                    { text: 'NO', onPress: () => console.log('Cancel Pressed') },
                ]
                // { cancelable: true }
            );
        } else {
            navigation.navigate('FinishScheduleModal', {
                schedule: schedule,
                thietBiSuDung: state.groups,
                type: type
            });
        }
    };
    const storeData = async (data) => {
        try {
            await AsyncStorage.setItem(`@equip_${schedule.yc_id}`, JSON.stringify(data));
        } catch (error) {
            // Error saving data
        }
    };
    return (
        <Container>
            <Content padder>
                <Card style={{ padding: 8 }}>
                    {((!schedule.MaThueBao || schedule.MaThueBao == '') && !schedule.TB_MATHUEBAO) ?
                        (<Item>
                            <Input
                                placeholder="Nhập mã số hợp đồng"
                                onChangeText={(contractNumber) =>
                                    setState({ ...state, contractNumber })
                                }
                                value={state.contractNumber}></Input>
                            <Button onPress={addGroup}>
                                <Text> + </Text>
                            </Button>
                        </Item>)
                        : null
                    }
                    {state.groups.map((item, index) => (
                        <CardGroupThietBi
                            group={item}
                            key={index}
                            showModal={showModalAddThietBi(index)}
                            removeGroup={removeGroup(index)}
                            removeItem={removeItem(index)}
                        />
                    ))}
                    {
                        type === 'indoor' ?
                            (<ModalAddIndoorEquip ref={(el) => modalAddThietBi.current = el} addItem={addItem} />) :
                            (<ModalAddThietBi ref={(el) => modalAddThietBi.current = el} addItem={addItem} />)
                    }
                    <Button
                        full
                        rounded
                        style={{ margin: 40, marginBottom: 10, marginTop: 20 }}
                        onPress={onSaveData}>
                        <Text>Lưu</Text>
                    </Button>
                </Card>
            </Content>
        </Container>
    );
}
export default SuppliesList