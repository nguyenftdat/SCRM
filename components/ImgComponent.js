import React from 'react';
import * as ImagePicker from 'expo-image-picker';
import {
    ListItem,
    Icon,
    Card,
    Text,
    Thumbnail
} from 'native-base';
import { styles } from '../styles/component/imgComponent';
import {
    TouchableOpacity
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import * as Permissions from 'expo-permissions';

const ImgComponent = React.forwardRef((props, ref) => {
    const [images, setImages] = React.useState({
        LocalImage: [],
        // multipleUrl: [],
        data: [],
    })
    const _pickImage = async () => {
        if (images.LocalImage.length < 3) {
            let pickerResult = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                base64: true,
                quality: 0.5,
                allowsEditing: false
            });
            let imageUri = pickerResult
                ? `data:image/jpg;base64,${pickerResult.uri}`
                : null;
            if (pickerResult.uri != undefined) {
                imageUri && { uri: imageUri };
                // images.multipleUrl.push(imageUri);
                setImages({
                    ...images,
                    LocalImage: images.LocalImage.concat([pickerResult.uri]),
                    data: images.data.concat([pickerResult.base64]),
                });
            }
            else {
                alert('Giới hạn 3 hình.')
            }
        }
    }
    const _takePhoto = async () => {
        if (images.LocalImage.length < 3) {
            const { status: cameraPerm } = await Permissions.askAsync(
                Permissions.CAMERA
            );
            const { status: cameraRollPerm } = await Permissions.askAsync(
                Permissions.CAMERA_ROLL
            );
            // only if user allows permission to camera AND camera roll
            if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
                let pickerResult = await ImagePicker.launchCameraAsync({
                    base64: true,
                    quality: 0.5,
                    allowsEditing: false,
                    // aspect: [4, 3],
                });
                if (!pickerResult.cancelled) {
                    let imageUri = pickerResult
                        ? `data:image/jpg;base64,${pickerResult.base64}`
                        : null;
                    // images.multipleUrl.push(imageUri);
                    // console.log(pickerResult.base64);
                    if (pickerResult.base64 !== undefined) {
                        // console.log(imageUri);
                        setImages({
                            ...images,
                            LocalImage: images.LocalImage.concat([pickerResult.uri]),
                            data: images.data.concat([pickerResult.base64]),
                        });
                    }
                }
            }
        }
        else {
            alert('Giới hạn 3 hình.')
        }
    }
    const _renderImages = () => {
        let image = [];
        images.LocalImage.map((item, index) => {
            image.push(
                <Col key={index}>
                    <ListItem>
                        <Thumbnail
                            square
                            large
                            key={index}
                            source={{ uri: item }}
                            style={{ height: 100, width: 100 }}
                        />
                        <TouchableOpacity style={{ position: 'absolute', top: 10, left: 0 }}
                            onPress={() => onRemove(item)}>
                            <Icon type="FontAwesome" name="times" />
                        </TouchableOpacity>
                    </ListItem>
                </Col>
            );
        });
        return image;
    }
    const onRemove = (item) => {
        let arr = images.LocalImage;
        let data_ = images.data;
        let index = arr.indexOf(item, 0);
        arr.splice(index, 1);
        data_.splice(index, 1);
        setImages({ ...images, LocalImage: arr, data: data_ });
    };
    const addImage = (images) => {
        props.addImage(images);
    }
    React.useEffect(() => {
        addImage(images);
    }, [images])
    React.useImperativeHandle(ref, () => ({

    }));
    return (
        <Card>
            <Grid>
                <Row style={styles.row}>
                    <Col style={styles.title}><Text style={styles.titleTxt}>HÌNH ẢNH</Text></Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            onPress={_pickImage}
                        >
                            <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-image`} style={{ fontSize: 50, margin: 5 }} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={_takePhoto}
                        >
                            <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-camera`} style={{ fontSize: 50, margin: 5 }} />
                        </TouchableOpacity>
                    </Col>
                </Row>
                <Row>
                    {_renderImages()}
                </Row>
            </Grid>
        </Card>
    );
});
export default ImgComponent;