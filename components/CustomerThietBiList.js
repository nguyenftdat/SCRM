//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import ListThietBiModel from '../models/ListThietBiModel';

type Props = {
  item: ListThietBiModel,
  index: number,
  onPressItem: (TTTB: ListThietBiModel) => void,
  callback: () => void
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          backgroundColor: backgroundColor,
          padding: 8,
        }}>
        <Text>
          ->. {props.item.TB_MATHUEBAO} - {props.item.TEN_DICHVU} (
          {props.item.GCCT_ID})
        </Text>
        <Text style={{ paddingLeft: 15 }}>Mã số 1: {props.item.MASO1}</Text>
        <Text style={{ paddingLeft: 15 }}>Mã số 2: {props.item.MASO2}</Text>
        <Text style={{ paddingLeft: 15 }}>
          Ngày bắt đầu: {props.item.NGAY_APDUNG_TEXT}
        </Text>
        <Text style={{ paddingLeft: 15 }}>
          Trạng thái: {props.item.TRANG_THAI}
        </Text>
        <Text style={{ paddingLeft: 15 }}>Ghi chú: {props.item.GHI_CHU}</Text>
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listTB: Array<ListThietBiModel>,
  onPressItem: (TTTB: ListThietBiModel) => void,
  callback: () => void
};
export const CustomerThietBiList = (props: CustomerListProps) => {
  return (
    <FlatList
      style={{padding: 8}}
      data={props.listTB}
      renderItem={({ item, index }) => {
        return (
          <CustomerListItem
            item={item}
            index={index}
            onPressItem={props.onPressItem}
            callback={props.callback}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};