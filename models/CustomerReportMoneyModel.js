export type CustomerReportMoneyModel = {
  BC_ID: number,
  KY_HIEU_PHIEUTHU: string,
  SO_PHIEUTHU: string,
  MA_KHACHHANG: string,
  TEN_KHACHHANG: string,
  TEN_COQUAN: string,
  DIACHI_LAPDAT: string,
  TONG_TIEN: number,
  Status: number,
};
