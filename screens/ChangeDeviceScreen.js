import React, { Component } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  View,
  Button,
  Text,
  Form,
  Picker,
  Item,
  Icon,
  Textarea,
  Input,
  Toast,
  DatePicker,
  Right,
  Left,
  Badge,
} from 'native-base';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  Alert,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import ChangeDevicePayload from '../models/ChangeDevicePayload';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { Grid, Row, Col } from 'react-native-easy-grid';
import OpenListMS1 from '../components/OpenListMS1';
import OpenListMS2 from '../components/OpenListMS2';
import search from '../assets/img/search_icon.png';
import EnterTheBranchWarehousePayload from '../models/EnterTheBranchWarehousePayload';
import { UserContext } from '../context/UserContext';
import SearchIcon from '../assets/img/search.png';
import MacCodeScanner from '../components/MacCodeScanner';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ChangeDeviceScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    hinhThuc: '',
    phieuXuat: '',
    ghiChu: '',
    trangThai: '',
    soHuu: '',
    boGiaiMa: '',
    thuHoiThiet: '',
    // ODF2: '',
    selectedItems: [],
    maSo1_M: '',
    maSo2_M: '',
  });
  const [data_1, setData_1] = React.useState({
    listDataMS: null,
  });
  const [data_2, setData_2] = React.useState({
    listDataMS: null,
  });
  const [disable, setDisable] = React.useState(false);
  const { TTTB, CN_ID, PGD_ID, schedule, callback } = route.params;
  const openListMS1 = React.useRef(null);
  const openListMS2 = React.useRef(null);
  const multiSelect = React.useRef(null);
  const macModem = React.useRef(null);
  React.useEffect(() => {
    resetState();
    // console.log(TTTB);
  }, []);

  const resetState = () => {
    setState({
      ...state,
      soHuu: global.listLich[0].TrangThaiSoHuu[0].Id,
      thuHoiThiet: global.listLich[0].LyDoThuHoi[0].Id,
      // ODF2: global.listLich[0].ListODF2[0].Id,
    });
  };
  const onValueChange_soHuu = (value: string) => {
    setState({ ...state, soHuu: value });
  };
  const onValueChange_boGiaiMa = (value: string) => {
    setState({ ...state, boGiaiMa: value });
  };
  const onValueChange_thuHoiThiet = (value: string) => {
    setState({ ...state, thuHoiThiet: value });
  };
  // const onValueChange_ODF2 = (value: string) => {
  //   setState({ ...state, ODF2: value });
  // };
  // TÌM KIẾM MÃ SỐ 1
  const addItemMaSo1 = (MS1: string) => {
    setState({ ...state, maSo1_M: MS1 });
  };
  const TKMaSo1 = () => {
    if (state.maSo1_M.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 1 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: state.maSo1_M,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_CHITIET,
        user.Token,
        payload,
        processTKMS1,
        null
      );
    }
  };
  const processTKMS1 = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setData_1({ ...data_1, listDataMS: data });
    }
  };
  const ktra1 = () => {
    if (data_1.listDataMS.length == 1) {
      setState({ ...state, maSo1_M: data_1.listDataMS[0].MaSo1 });
    } else if (data_1.listDataMS.length > 1) {
      openListMS1.current.showModal();
    } else {
      Alert.alert('Khong tim thay ma so 1 ban yeu cau');
    }
  };
  //TÌM KIẾM MÃ SỐ 2
  const addItemMaSo2 = (MS2: string) => {
    setState({ ...state, maSo2_M: MS2 });
  };
  const TKMaSo2 = () => {
    if (state.maSo2_M.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 2 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '2',
        Entry: state.maSo2_M,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_CHITIET,
        user.Token,
        payload,
        processTKMS2,
        null
      );
    }
  };
  const processTKMS2 = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setData_2({ ...data_2, listDataMS: data });
    }
  };
  const ktra2 = () => {
    if (data_2.listDataMS.length == 1) {
      setState({ ...state, maSo2_M: data_2.listDataMS[0].MaSo2 });
    } else if (data_2.listDataMS.length > 1) {
      openListMS2.current.showModal();
    } else {
      Alert.alert('Khong tim thay ma so 2 ban yeu cau');
    }
  };
  React.useEffect(() => {
    if (data_1.listDataMS !== null) {
      ktra1();
    }
  }, [data_1]);
  React.useEffect(() => {
    if (data_2.listDataMS !== null) {
      ktra2();
    }
  }, [data_2]);
  const onSelectedItemsChange = (selectedItems) => {
    setState({ ...state, selectedItems: selectedItems });
  };
  const onSubmit = () => {
    if (state.maSo1_M == '' && state.maSo2_M == '') {
      Alert.alert(
        'Thông tin thiết bị mới mã số 1 hoặc mã số 2 không được để rỗng'
      );
    } else {
      setDisable(true);
      const payload: ChangeDevicePayload = {
        TBTB_ID: TTTB.TBTB_ID,
        CN_ID: CN_ID,
        PGD_ID: PGD_ID,
        TT_SoHuu: state.soHuu,
        MaSo1Moi: state.maSo1_M,
        MaSo2Moi: state.maSo2_M,
        LyDoThuHoi: state.thuHoiThiet,
        // ODF2: state.selectedItems[0].toString(),
        PhieuXuat: state.phieuXuat,
        GhiChu: state.ghiChu,
      };
      callAPI(
        ApiMethods.DOITHIETBI,
        user.Token,
        payload,
        processSubmit,
        errorSubmit
      );
    }
  };
  const processSubmit = (data) => {
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    storeOldMacWifi(TTTB.MASO1);
    storeNewMacWifi(state.maSo1_M);
    callback();
    navigation.pop();
  };
  const errorSubmit = (error) => {
    alert(error.message);
    setDisable(false);
  };
  const storeNewMacWifi = async (value) => {
    try {
      await AsyncStorage.setItem(`@new_Modem_${schedule.yc_id}`, value);
    } catch (e) {
      // saving error
    }
  };
  const storeOldMacWifi = async (value) => {
    try {
      await AsyncStorage.setItem(`@old_Modem_${schedule.yc_id}`, value);
    } catch (e) {
      // saving error
    }
  };

  const addMAC = (value) => {
    setState({ ...state, maSo1_M: value })
  };
  return (
    <Container>
      <Content padder>
        <Grid style={styles.grid}>
          <Row style={styles.row}>
            <Col style={{ alignItems: 'center' }}>
              <Text style={styles.title}>Thiết bị cần đổi</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Trạng thái</Text>
            </Col>
            <Col>
              <Text>{TTTB.TRANG_THAI}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Mã số 1</Text>
            </Col>
            <Col>
              <Text>{TTTB.MASO1}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Mã số 2</Text>
            </Col>
            <Col>
              <Text>{TTTB.MASO2}</Text>
            </Col>
          </Row>
        </Grid>
        <Grid style={styles.grid}>
          <Row style={styles.row}>
            <Col style={{ alignItems: 'center' }}>
              <Text style={styles.title}>Thiết bị mới</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Sở hữu</Text>
            </Col>
            <Col>
              <Form style={styles.formStyle}>
                <Item Picker style={{ marginLeft: 0, paddingLeft: 10 }}>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    selectedValue={state.soHuu}
                    onValueChange={onValueChange_soHuu}>
                    {global.listLich[0].TrangThaiSoHuu.map((item, index) => {
                      return (
                        <Picker.Item
                          label={item.Text}
                          value={item.Id}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                </Item>
              </Form>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Mã số 1</Text>
            </Col>
            <Col>
              <Form style={styles.formStyle}>
                <Item>
                  <Input
                    placeholder="Nhập mã số"
                    value={state.maSo1_M}
                    onChangeText={(text) => {
                      setState({ ...state, maSo1_M: text });
                    }}
                  />
                  <TouchableOpacity onPress={() => macModem.current.showModal()}>
                    <Icon name="crop-free" type='MaterialIcons' />
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.searchBtn} onPress={TKMaSo1}>
                    <Icon name="search" type='MaterialIcons' />
                  </TouchableOpacity>
                </Item>
              </Form>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Mã số 2</Text>
            </Col>
            <Col>
              <Form style={styles.formStyle}>
                <Item>
                  <Input
                    placeholder="Nhập mã số"
                    value={state.maSo2_M}
                    onChangeText={(text) => {
                      setState({ ...state, maSo2_M: text });
                    }}
                  />
                  <TouchableOpacity style={styles.searchBtn} onPress={TKMaSo2}>
                    <Thumbnail small source={SearchIcon} />
                  </TouchableOpacity>
                </Item>
              </Form>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Thu hồi thiết bị</Text>
            </Col>
            <Col>
              <Form style={styles.formStyle}>
                <Item Picker style={{ marginLeft: 0, paddingLeft: 10 }}>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    selectedValue={state.thuHoiThiet}
                    onValueChange={onValueChange_thuHoiThiet}>
                    {global.listLich[0].LyDoThuHoi.map((item, index) => {
                      return (
                        <Picker.Item
                          label={item.Text}
                          value={item.Id}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                </Item>
              </Form>
            </Col>
          </Row>
          {/* <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Đổi ODF2</Text>
            </Col>
            <Col>
              <MultiSelect
                hideTags
                single={true}
                fontSize={15}
                items={global.listLich[0].ListODF2}
                uniqueKey="Id"
                ref={multiSelect}
                onSelectedItemsChange={onSelectedItemsChange}
                selectedItems={state.selectedItems}
                selectText="Chọn ODF2"
                searchInputPlaceholderText="Tìm kiếm ODF2..."
                // altFontFamily="ProximaNova-Light"
                tagRemoveIconColor="#CCC"
                tagTextColor="#CCC"
                textColor="#000"
                selectedItemTextColor="#CCC"
                selectedItemIconColor="#CCC"
                itemTextColor="#000"
                displayKey="Text"
                searchInputStyle={{ color: '#000' }}></MultiSelect>
            </Col>
          </Row> */}
        </Grid>
        <Grid style={styles.grid}>
          <Row style={styles.row}>
            <Col style={{ alignItems: 'center' }}>
              <Text style={styles.title}>Thông tin đổi</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Phiếu xuất</Text>
            </Col>
            <Col>
              <Textarea
                style={styles.inputStyle}
                rowSpan={2}
                onChangeText={(text) => {
                  setState({ ...state, phieuXuat: text });
                }}
              />
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.leftTxt}>Ghi chú</Text>
            </Col>
            <Col>
              <Textarea
                style={styles.inputStyle}
                rowSpan={2}
                onChangeText={(text) => {
                  setState({ ...state, ghiChu: text });
                }}
              />
            </Col>
          </Row>
        </Grid>
        <Button full rounded onPress={onSubmit} style={styles.btn_save} disabled={disable}>
          <Text style={{ color: 'white' }}>Thực hiện</Text>
        </Button>
      </Content>
      <OpenListMS1
        ref={(el) => (openListMS1.current = el)}
        datalist={data_1.listDataMS}
        addItemMaSo1={addItemMaSo1}
      />
      <OpenListMS2
        ref={(el) => (openListMS2.current = el)}
        datalist={data_2.listDataMS}
        addItemMaSo2={addItemMaSo2}
      />
      <MacCodeScanner
        ref={(el) => (macModem.current = el)}
        addMAC={addMAC}
      />
    </Container>
  );
};

export default ChangeDeviceScreen;
const styles = StyleSheet.create({
  inputStyle: {
    borderWidth: 0.5,
  },
  formStyle: {
    borderWidth: 1,
    borderRadius: 2,
  },
  btn_save: {
    margin: 40,
    marginTop: 10,
  },
  row: {
    marginBottom: 15,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  leftTxt: {
    color: 'gray',
  },
  grid: {
    borderWidth: 0.5,
    marginBottom: 10,
    padding: 10,
  },
});
