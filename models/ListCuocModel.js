export type ListCuocModel = {
  RowNumber: number,
  T_TEN_THANG: string,
  TB_ID: number,
  TB_MATHUEBAO: string,
  TEN_DICHVU: string,
  TONG_NO: number,
  TONG_DATHU: number,
  TONG_CHUATHU: number,
  TONG_NO_TEXT: string,
  TONG_DATHU_TEXT: string,
  TONG_CHUATHU_TEXT: string,
};
