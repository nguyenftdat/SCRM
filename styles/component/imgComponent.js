import { StyleSheet, Dimensions } from "react-native";
const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
    inputStyle: {
        borderBottomWidth: 0.5,
        borderColor: 'rgba(0,0,0,0.4)',
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        // margin: 2,
        width: Dimensions.get('window').width * 0.9,
        height: Dimensions.get('window').width * 0.8,
    },
    leftBlock: {
        width: Dimensions.get('window').width * 0.35,
        justifyContent: 'center',
    },
    row: {
        marginTop: 10,
    },
    mapBlock: {
        marginTop: 10,
        // borderWidth: 0.5,
        height: Dimensions.get('window').width * 0.8,
        width: Dimensions.get('window').width * 0.9,
    },
    titleTxt: {
        fontWeight: 'bold',
        // fontSize: 13
    },
    btnMap: {
        position: 'absolute',
        top: 4,
        right: 9,
        opacity: 0.7,
    },
    dropdown: {
        marginLeft: 0,
        paddingLeft: 0,
    },
    title: {
        alignItems: 'center'
        // margin: 10
    },
    headerModal: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        borderBottomWidth: 0.5,
        width: Dimensions.get('window').width * 0.8,
    },
    modalText: {
        fontWeight: 'bold',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    modalView: {
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 5,
    },
    footerModal: {
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 10
    },
    contentModal: {
        maxHeight: 250
    }
});
export { styles }