//@flow
import React, { Component } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  Toast,
  Input,
  Text,
  Label,
  View,
  Item,
  Form,
  Picker,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Button,
  Card,
} from 'native-base';
import {
  TextInput,
  Alert,
  StyleSheet,
  FlatList,
  Dimensions,
} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import { CalendarTransferPayload } from '../models/CalendarTransferPayload';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import { Grid, Col, Row } from 'react-native-easy-grid';

const CalendarTransferScreens = ({ route, navigation }) => {
  let multiSelect = null;
  const { user, setUser } = React.useContext(UserContext);
  const schedule = route.params.schedule;
  const [state, setState] = React.useState({
    NVNhan: undefined,
    PGD: undefined,
    selectedItems: [],
  });
  const [variable, setVariable] = React.useState({
    listNhanVien: [],
    mangMaYeuCau: [],
  });
  let listNhanVien = [];
  let mangMaYeuCau = [];
  React.useEffect(() => {
    resetState();
  }, []);
  const resetState = () => {
    setState({ ...state, PGD: global.listLich[0].DanhMucPGD[0].Id });
    listNhanVien = global.listLich[0].NhanVienTheoLich.filter((data) => {
      return (
        data.Reference ===
        global.listLich[0].DanhMucPGD.find((element) => {
          if (element.Id === global.listLich[0].DanhMucPGD[0].Id)
            return element;
        }).Id
      );
    });
    mangMaYeuCau.push(schedule.ma_yeucau);
    setVariable({ ...variable, listNhanVien: listNhanVien, mangMaYeuCau: mangMaYeuCau });
  };
  const onValueChange_PGD = (value: string) => {
    setState({ ...state, PGD: value });
    listNhanVien = global.listLich[0].NhanVienTheoLich.filter((data) => {
      return (
        data.Reference ===
        global.listLich[0].DanhMucPGD.find((element) => {
          if (element.Id === value) return element;
        }).Id
      );
    });
    setVariable({ ...variable, listNhanVien: listNhanVien });
  };
  const onSubmit = () => {
    if (state.selectedItems.length == 0) {
      Alert.alert('Thông báo', 'Vui lòng chọn nhân viên');
    } else {
      const payload: CalendarTransferPayload = {
        listMaYeuCau: variable.mangMaYeuCau,
        nvNhan: state.selectedItems[0].toString(),
      };
      // Alert.alert(JSON.stringify(payload));
      callAPI(ApiMethods.SAP_LICH, user.Token, payload, processSubmit, null);
    }
  };
  const processSubmit = (data) => {
    global.refreshHome();
    Toast.show({
      text: 'Chuyển lịch thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.navigate('Home');
  };

  const onSelectedItemsChange = (selectedItems) => {
    setState({ ...state, selectedItems: selectedItems });
  };

  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Phòng giao dịch:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Form>
                  <Item Picker style={{ marginLeft: 0 }}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.PGD}
                      onValueChange={onValueChange_PGD}>
                      {global.listLich[0].DanhMucPGD.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Nhân viên nhận:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <MultiSelect
                  hideTags
                  single={true}
                  fontSize={15}
                  items={
                    variable.listNhanVien.length > 0
                      ? variable.listNhanVien
                      : []
                  }
                  uniqueKey="Id"
                  ref={(component) => {
                    multiSelect = component;
                  }}
                  onSelectedItemsChange={onSelectedItemsChange}
                  selectedItems={state.selectedItems}
                  selectText="Chọn nhân viên"
                  searchInputPlaceholderText="Tìm kiếm nhân viên..."
                  // altFontFamily="ProximaNova-Light"
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  textColor="#000"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="Text"
                  searchInputStyle={{ color: '#000' }}></MultiSelect>
              </Col>
            </Row>
          </Grid>
          <Button full rounded onPress={onSubmit} style={styles.btn_chuyenlich}>
            <Text>Sắp lịch</Text>
          </Button>
        </Card>
      </Content>
    </Container>
  );
};
export default CalendarTransferScreens;
const styles = StyleSheet.create({
  textStyle: {
    paddingLeft: 5,
    fontWeight: 'bold',
    fontSize: 14
  },
  btn_chuyenlich: {
    margin: 40,
    marginTop: 10,
    marginBottom: 20,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.35,
    justifyContent: 'center',
  },
  row: {
    padding: 10,
    paddingLeft: 5,
  },
  rightBlock: {
    paddingLeft: 10,
    justifyContent: 'center',
  },
});
