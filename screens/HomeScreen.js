//React import here
import * as React from 'react';
import {
  StatusBar,
  StyleSheet,
  Alert,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
//External imports here
import {
  Container,
  Header,
  Body,
  Content,
  Button,
  View,
  Text,
  Left,
  Right,
  Icon,
  Title,
  Spinner,
  Card,
  Toast
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
//Custom imports here
import { UserContext } from '../context/UserContext';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { ScheduleList } from '../components/ScheduleList';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';

const HomeScreen = ({ route, navigation }) => {
  //Context
  const { user, setUser } = React.useContext(UserContext);
  //Refs
  const intervalId = React.useRef(0);
  //State
  const [state, setState] = React.useState({
    listSchedule: null,
    loading: true,
    countQH: 0,
    countDH: 0,
    countLD: 0,
    countBT: 0,
  });
  const [refreshing, setRefreshing] = React.useState(false);

  const [expoPushToken, setExpoPushToken] = React.useState(undefined);
  const [notification, setNotification] = React.useState(false);
  const notificationListener = React.useRef();
  const responseListener = React.useRef();

  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      // Alert.alert(token);
      savePushToken(token);
      // console.log(token);
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }

    // return token;
  }


  global.refreshHome = () => {
    getListSchedule();
  };
  //Filter lich
  const onDetailDenHan = () => {
    let list = global.listSchedule.filter((data) => {
      return data.Value >= 120 && data.Value < 240;
    });
    setState({
      ...state,
      listSchedule: list,
    });
  };
  const onDetailQuaHan = () => {
    let list = global.listSchedule.filter((data) => {
      return data.Value >= 240;
    });
    setState({
      ...state,
      listSchedule: list,
    });
  };
  const onDetailBaoTri = () => {
    let list = global.listSchedule.filter((data) => {
      return data.Reference === 'BT';
    });
    setState({
      ...state,
      listSchedule: list,
    });
  };
  const onDetailLapMoi = () => {
    let list = global.listSchedule.filter((data) => {
      return data.Reference === 'LD';
    });
    setState({
      ...state,
      listSchedule: list,
    });
  };

  React.useEffect(() => {
    const payload = { CN_ID: user.CN_ID };
    callAPI(
      ApiMethods.GET_LIST_MATERIAL,
      user.Token,
      payload,
      processmaterial,
      null
    );
    callAPI(
      ApiMethods.GET_DANH_MUC_XUAT_KHO,
      user.Token,
      payload,
      processXuatKho,
      null
    );
    callAPI(
      ApiMethods.GET_LIST_REASONS,
      user.Token,
      payload,
      processFinish,
      null
    );
    callAPI(
      ApiMethods.GET_DANH_MUC_ALL,
      user.Token,
      payload,
      processMucLich,
      null
    );
    getListSchedule();
    intervalId.current = setInterval(() => {
      getListSchedule();
    }, 300000);
    // Specify how to clean up after this effect:
    return function cleanup() {
      clearInterval(intervalId.current);
    };
  }, []);

  React.useEffect(() => {
    
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    // // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {

      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  const savePushToken = (token) => {
    let payload = {
      PushToken: token
    }
    // Alert.alert(payload.PushToken);
    callAPI(
      ApiMethods.SAVE_PUSH_TOKEN,
      user.Token,
      payload,
      processData,
      null
    )
  }
  const processData = () => {
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
  }

  const processmaterial = (data) => {
    global.listMaterial = data;
  };
  const processXuatKho = (data) => {
    global.listXuatKho = data;
  };
  const processFinish = (data) => {
    global.listFinish = data;
  };
  const processMucLich = (data) => {
    global.listLich = data;
    global.vatTuIndoor = data[0].DanhMucVatTuInDoor;
    global.DanhMucThietBiWifi = data[0].DanhMucThietBiWifi;
    global.DsVatTuIndoor = global.DanhMucThietBiWifi.filter((element) => {
      return Number(element.Id) > 0;
    });
    global.vatTuIndoor.forEach((item, index) => {
      global.DsVatTuIndoor.push(item);
    });
    // console.log(global.vatTuIndoor);
    // console.log(global.DsVatTuIndoor);
  };

  const getListSchedule = () => {
    setState({ ...state, loading: true });
    callAPI(
      ApiMethods.GET_LIST_MY_SCHEDULES,
      user.Token,
      '',
      processResult,
      processError
    );
  };
  const processResult = (data) => {
    let countBT = 0,
      countLM = 0,
      countQH = 0,
      countDH = 0;
    data.forEach((element) => {
      if (element.Reference == 'LD') countLM++;
      else countBT++;
      if (element.Value >= 240) {
        countQH++;
      } else if (element.Value >= 120 && element.Value < 240) {
        countDH++;
      }
    });
    setState({
      listSchedule: data,
      loading: false,
      countQH: countQH,
      countDH: countDH,
      countLD: countLM,
      countBT: countBT,
    });
    global.listSchedule = data;
  };

  const processError = (error) => {
    setState({
      ...state,
      loading: false,
    });
  };

  const refreshControl = () => {
    setState({
      ...state,
      listSchedule: global.listSchedule,
    });
  };

  const onPressItem = (id) => {
    navigation.navigate('DetailedSchedule', {
      scheduleId: id,
      note: 'personal_calendar',
    });
  };
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getListSchedule();
    wait(1000).then(() => setRefreshing(false));
  }, []);
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  return (
    <Container>
      <Content
        padder
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <Grid>
          <Col>
            <TouchableOpacity onPress={onDetailQuaHan}>
              <Card style={[styles.quaHan, styles.btn]}>
                <Text style={styles.textBtn_1}>{state.countQH}</Text>
                <Text style={styles.textBtn_2}>QUÁ HẠN</Text>
              </Card>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity onPress={onDetailDenHan}>
              <Card style={[styles.sapDenHan, styles.btn]}>
                <Text style={styles.textBtn_1}>{state.countDH}</Text>
                <Text style={styles.textBtn_2}>ĐẾN HẠN</Text>
              </Card>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity onPress={onDetailLapMoi}>
              <Card style={[styles.moiGiao, styles.btn]}>
                <Text style={styles.textBtn_1}>{state.countLD}</Text>
                <Text style={styles.textBtn_2}>LẮP ĐẶT</Text>
              </Card>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity onPress={onDetailBaoTri}>
              <Card style={[styles.henLai, styles.btn]}>
                <Text style={styles.textBtn_1}>{state.countBT}</Text>
                <Text style={styles.textBtn_2}>BẢO TRÌ</Text>
              </Card>
            </TouchableOpacity>
          </Col>
        </Grid>
        {state.loading ? (
          <Spinner />
        ) : (
          <ScheduleList
            listSchedule={state.listSchedule}
            onPressItem={onPressItem}
          />
        )}
      </Content>
    </Container>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10,
    paddingBottom: 10,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn_1: {
    fontSize: 20,
    color: 'white',
    fontWeight: '700',
  },
  textBtn_2: {
    color: 'white',
    fontSize: 12,
    fontWeight: '700',
  },
  quaHan: {
    backgroundColor: '#c00000',
  },
  sapDenHan: {
    backgroundColor: '#ffc000',
  },
  moiGiao: {
    backgroundColor: '#fac090',
  },
  henLai: {
    backgroundColor: '#95b3d7',
  },
});
