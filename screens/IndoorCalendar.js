//React import here
import * as React from 'react';
import {
  StatusBar,
  StyleSheet,
  Alert,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
//External imports here
import {
  Container,
  Header,
  Body,
  Content,
  Button,
  View,
  Text,
  Left,
  Right,
  Icon,
  Title,
  Spinner,
  Card,
  Item,
  Input,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
//Custom imports here
import { UserContext } from '../context/UserContext';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { ScheduleList } from '../components/ScheduleList';

const IndoorCalendar = ({ route, navigation }) => {
  //Context
  const { user, setUser } = React.useContext(UserContext);
  //Refs
  const intervalId = React.useRef(0);
  //State
  const [state, setState] = React.useState({
    listIndoorSchedule: null,
    loading: true,
    countQH: 0,
    countDH: 0,
    countLD: 0,
    countBT: 0,
  });
  const [refreshing, setRefreshing] = React.useState(false);
  global.refreshIndoorHome = () => {
    getListSchedule();
  };

  React.useEffect(() => {
    getListSchedule();
    intervalId.current = setInterval(() => {
      getListSchedule();
    }, 300000);
    // Specify how to clean up after this effect:
    return function cleanup() {
      clearInterval(intervalId.current);
    };
  }, []);

  const getListSchedule = () => {
    setState({ ...state, loading: true });
    callAPI(
      ApiMethods.GET_LIST_MY_SCHEDULES_INDOOR,
      user.Token,
      '',
      processResult,
      processError
    );
  };
  const processResult = (data) => {
    global.listIndoorSchedule = data;
    setState({
      listIndoorSchedule: data,
      loading: false,
    });
  };

  const processError = (error) => {
    setState({
      ...state,
      loading: false,
    });
  };


  const onPressItem = (id) => {
    navigation.navigate('DetailedIndoorSchedule', {
      scheduleId: id,
      note: 'personal_calendar'
    });
  };
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getListSchedule();
    wait(1000).then(() => setRefreshing(false));
  }, []);
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  return (
    <Container>
      <Header searchBar rounded>
        <Item>
          <Input
            placeholder="Nhập thông tin tìm kiếm"
            onChangeText={(text) => {
              let upperList = global.listIndoorSchedule.filter((data) => data.Text.includes(text.toUpperCase()));
              let lowerList = global.listIndoorSchedule.filter((data) => data.Text.includes(text.toLowerCase()));
              if((text.toUpperCase() !== text.toLowerCase()) && text.length > 1){
                setState({ ...state, 
                  listIndoorSchedule: upperList.concat(lowerList)
                })
              }
              else {
                setState({ ...state, 
                  listIndoorSchedule: upperList
                })
              }
            }}
            returnKeyType='done'
          />
        </Item>
      </Header>
      <Content
        padder
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        { global.listIndoorSchedule === null || (state.listIndoorSchedule && state.listIndoorSchedule.length < 1) ? 
          (<View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontStyle: 'italic', fontWeight: 'bold'}}>Không có lịch tồn tại.</Text> 
          </View>)
          : null
        }
        {state.loading ? (
          <Spinner />
        ) : (
          <ScheduleList
            listSchedule={state.listIndoorSchedule}
            onPressItem={onPressItem}
          />
        )}
      </Content>
    </Container>
  );
};

export default IndoorCalendar;

const styles = StyleSheet.create({
  btn: {
    paddingTop: 10, 
    paddingBottom: 10,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textBtn_1: {
    fontSize: 20,
    color: 'white',
  },
  textBtn_2: {
    color: 'white',
    fontSize: 15,
  },
  quaHan: {
    backgroundColor: '#c00000',
  },
  sapDenHan: {
    backgroundColor: '#ffc000',
  },
  moiGiao: {
    backgroundColor: '#fac090',
  },
  henLai: {
    backgroundColor: '#95b3d7',
  },
});
