//@flow
import React from 'react';
import { Text, View, Card } from 'native-base';
import { StyleSheet, Dimensions } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { ScheduleHistoryModel } from '../models/ScheduleHistoryModel';
import { formatDate } from '../utils/Utils';
const FlatListItem = (props) => {
  const item: ScheduleHistoryModel = props.item;
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#e9edf4';
  return (
    <Row style={styles.row}>
      <Col style={[styles.leftBlock, {backgroundColor: backgroundColor}]}>
        <Text>
          {item.ketqua_xuly_ten == null
            ? item.ma_ketqua_xuly
            : item.ketqua_xuly_ten}
        </Text>
      </Col>
      <Col style={[styles.middleBlock, {backgroundColor: backgroundColor}]}>
        <Text>{formatDate(new Date(item.ngay_xuly))}</Text>
      </Col>
      <Col style={[styles.rightBlock, {backgroundColor: backgroundColor}]}>
        <Text>{item.ten_nguoi_xuly}</Text>
      </Col>
    </Row>
  );
};

export const HistoryComponent = (props) => {
  if (props.history === null || props.history === undefined) {
    return <View />;
  }
  const history: Array<any> = props.history;
  if (history.length === 0) return <View />;
  else {
    return (
      <Card style={{padding: 2}}>
        <Grid>
          <Row style={styles.row}>
            <Col style={[styles.leftBlock, styles.background]}>
              <Text>Thao tác</Text>
            </Col>
            <Col style={[styles.middleBlock, styles.background]}>
              <Text>Ngày thao tác</Text>
            </Col>
            <Col style={[styles.rightBlock, styles.background]}>
              <Text>Người thao tác</Text>
            </Col>
          </Row>
          {history.map((item, index) => {
            return (
              <FlatListItem item={item} index={index} key={'His' + index} />
            );
          })}
        </Grid>
      </Card>
    );
  }
};
const styles = StyleSheet.create({
  row: {
    padding: 1
  },
  leftBlock: {
    marginRight: 1,
    padding: 2,
    paddingTop: 5,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  middleBlock: {
    marginRight: 1,
    marginLeft: 1,
    padding: 2,
    paddingTop: 5,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rightBlock: {
    marginLeft: 1,
    padding: 2,
    paddingTop: 5,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  background: {
    backgroundColor: '#f0ad4e',
    alignItems: 'center'
  }
});