export type EnterTheBranchWarehousePayload = {
  Type: string,
  Entry: string,
};
