import React, { Component } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  Toast,
  Input,
  Text,
  View,
  Item,
  Form,
  Picker,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Button,
  Header,
  Card
} from 'native-base';
import {
  StyleSheet,
  Dimensions,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Modal,
} from 'react-native';
import closeIcon from '../assets/img/close_icon.png';

const FlatListItem = ({ onClose, addMS2, item, index }) => {
  return (
    <TouchableOpacity
      onPress={() => {
        addMS2(item.MaSo2);
        onClose();
      }}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          backgroundColor: index % 2 == 0 ? '#d0d8e8' : '#e9edf4',
          margin: 5,
          padding: 8,
          borderRadius: 2,
          justifyContent: 'center'
        }}>
        <Text style={{ paddingLeft: 10, fontSize: 20 }}>{item.MaSo2}</Text>
      </View>
    </TouchableOpacity>
  );
};

const OpenListMS2 = React.forwardRef((props, ref) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  const addMS2 = (MS2: string) => {
    props.addItemMaSo2(MS2);
  };
  const onClose = () => {
    setModalVisible(false);
  };
  React.useImperativeHandle(ref, () => ({
    showModal(index) {
      setModalVisible(true);
    },
  }));
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <TouchableOpacity
            style={styles.closeBtn}
            onPress={() => {
              setModalVisible(!modalVisible);
            }}>
            <Thumbnail
              square
              source={closeIcon}
              style={{ width: 30, height: 30 }}
            />
          </TouchableOpacity>
          <View style={{ justifyContent: 'center', margin: 10 }}>
            <Text style={{ fontSize: 20 }}>KẾT QUẢ TÌM KIẾM MÃ SỐ 2</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <ScrollView style={styles.scrollContainer}>
              <Form>
                <FlatList
                  data={props.datalist}
                  renderItem={({ item, index }) => {
                    return (
                      <FlatListItem
                        onClose={onClose}
                        addMS2={addMS2}
                        item={item}
                        index={index}></FlatListItem>
                    );
                  }}
                  keyExtractor={(item, index) => 'key' + index}></FlatList>
              </Form>
            </ScrollView>
          </View>
        </View>
      </View>
    </Modal>
  );
});
export default OpenListMS2;
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 22,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    height: Dimensions.get('window').height * 0.6,
    margin: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeBtn: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 5,
  },
});
