export type ChangeDevicePayload = {
  TBTB_ID: number,
  CN_ID: number,
  PGD_ID: number,
  TT_SoHuu: string,
  MaSo1Moi: string,
  MaSo2Moi: string,
  LyDoThuHoi: string,
  ODF2: string,
  PhieuXuat: string,
  GhiChu: string,
};
