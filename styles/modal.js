import { StyleSheet, Dimensions } from "react-native";
const { width } = Dimensions.get('window');
const qrSize = width * 0.7;
const modal_styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    leftBlock: {
        width: Dimensions.get('window').width * 0.35,
        justifyContent: 'center',
        padding: 16
    },
    rightBlock: {
        width: Dimensions.get('window').width * 0.45,
        justifyContent: 'center',
        margin: 16
    },
    row: {
        borderBottomWidth: 0.5,
    },
    mainModalView: {
        // height: Dimensions.get('window').height * 0.8,
        // width: Dimensions.get('window').height * 0.6,
        margin: 10,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    titleTxt: {
        fontWeight: 'bold',
        color: 'grey',
        marginLeft: 10,
    },
    closeBtn: {
        position: 'absolute',
        top: 3,
        right: 8,
        zIndex: 5,
    },
    qr: {
        width: qrSize,
        height: qrSize,
    },
    cancel: {
        fontSize: width * 0.05,
        textAlign: 'center',
        width: '70%',
        color: 'white',
        position: 'absolute',
        bottom: '15%',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerModal: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        borderBottomWidth: 0.5,
        width: Dimensions.get('window').width * 0.8,
    },
    contentModal: {
        minHeight: 200,
        // maxHeight: Dimensions.get('window').height * 0.8,
        // position: 'relative'
    },
    closeBtn: {
        position: 'absolute',
        top: 5,
        right: 5,
        // zIndex: 5,
    },
    block: {
        height: 50
    },
    footer: {
        // alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    }
});
export { modal_styles }