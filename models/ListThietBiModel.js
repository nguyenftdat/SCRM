//@flow
export type ListThietBiModel = {
  RowNumber: number,
  TB_ID: number,
  TB_MATHUEBAO: string,
  GC_ID: string,
  GCCT_ID: string,
  MASO1: string,
  MASO2: string,
  MASO3: string,
  NGAY_APDUNG_TEXT: string,
  TRANG_THAI: string,
  GHI_CHU: string,
  TEN_DICHVU: string,
  TBTB_ID: number,
};
