import { StyleSheet, Dimensions } from "react-native";
const { width } = Dimensions.get('window');
const stylesLib = StyleSheet.create({
    l_block_w_3: {
        width: width * 0.3,
        justifyContent: 'center',
    },
    l_block_w_4: {
        width: width * 0.4,
        justifyContent: 'center',
    },
    mt_10: {
        marginTop: 10,
    },
    mb_10: {
        marginBottom: 10,
    },
    ml_10: {
        marginLeft: 10,
    },
    mr_10: {
        marginRight: 10,
    },
    mt_15: {
        marginTop: 15,
    },
    mb_15: {
        marginBottom: 15,
    },
    ml_15: {
        marginLeft: 15,
    },
    mr_15: {
        marginRight: 15,
    },
    p_10: {
        padding: 10
    },
    b_txt: {
        fontWeight: 'bold',
    },
    ct: {
        alignItems: 'center'
    },
    italic_txt: {
        fontStyle: 'italic'
    },
    small_txt: {
        fontSize: 15
    },  
    grid: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
    },
    border_1: {
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        marginBottom: 5
    }
});
export { stylesLib }
// l: left
// m: margin
// b: bold
// ct: center