import React, { Component } from 'react';
import {
    Container,
    Content,
    Toast,
    Input,
    Text,
    Label,
    View,
    Button,
    Textarea,
    Thumbnail,
    Accordion,
    Card,
    CheckBox,
    Spinner
} from 'native-base';
import {
    TextInput,
    StyleSheet,
    FlatList,
    Dimensions,
    Alert,
    Image,
    TouchableWithoutFeedback
} from 'react-native';
import Checkbox from 'expo-checkbox';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { UserContext } from '../context/UserContext';

export default function PayUpList({ route, navigation }) {
    const { user, setUser } = React.useContext(UserContext);
    let data = route.params.data;
    const [state, setState] = React.useState({ data: data, TotalAmount: 0 });
    const [disable, setDisable] = React.useState({btnStatus: true, spinnerStatus: false}); // btnStatus: disabled cho button THU TIEN, status cho Spinner
    React.useEffect(() => {
        data.forEach((item, key) => {
            item.isChecked = true;
        });
        let sum = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i].isChecked == true) {
                sum = sum + Number(data[i].Amount);
            }
        }
        setState({ ...state, data: data, TotalAmount: sum.toFixed(4) });
        // console.log(data);
    }, [])
    React.useEffect(() => {
        if (state.TotalAmount > 0) {
            setDisable({...disable, btnStatus: false});
        }
        else {
            setDisable({...disable, btnStatus: true});
        }
    }, [state.TotalAmount])
    const onGetBill = (key) => {
        let sum = 0;
        if (data[key].isChecked == false) {
            for (let i = 0; i <= key; i++) {
                data[i].isChecked = true;
            }
        }
        else {
            for (let i = 0; i <= data.length - key - 1; i++) {
                data[key + i].isChecked = false;
            }
        }
        // console.log(data);
        for (let i = 0; i < data.length; i++) {
            if (data[i].isChecked == true) {
                sum = sum + Number(data[i].Amount);
            }
        }
        setState({ ...state, data: data, TotalAmount: sum.toFixed(4) });
    };

    const onPayBill = () => {
        setDisable({...disable, btnStatus: true, spinnerStatus: true});
        let debt = [];
        state.data.forEach((item, index) => {
            if (item.isChecked == true) {
                let temp = {
                    DebtMonth: item.DebtMonth,
                    DebtMonthId: item.DebtMonthId,
                    Amount: item.Amount
                }
                debt.push(temp);
                // console.log(debt);
            }
        })
        let payload = {
            CustCode: state.data[0].CustCode,
            InvestorBranchId: state.data[0].BranchCode,
            TotalAmount: state.TotalAmount,
            Debts: debt
        }
        // console.log(payload);
        callAPI(ApiMethods.PAY_BILL, user.Token, payload, processSubmit, null);
    }
    const processSubmit = (data) => {
        Toast.show({
            text: 'Cập nhật thành công',
            duration: 2000,
            position: 'bottom',
            textStyle: { textAlign: 'center' },
        });
        navigation.navigate('BarcodeScreen');
    };
    return (
        <Container>
            <Content padder>
                <Grid>
                    {state.data.map((item, key) =>
                        <TouchableWithoutFeedback key={key} onPress={() => onGetBill(key)}>
                            <Row style={styles.row}>
                                <Col style={styles.leftBlock}>
                                    <Text style={styles.title}>{item.BillInfo}</Text>
                                    <Text style={styles.name}>Khách hàng: {item.CustName}</Text>
                                    <Text style={styles.billInfo}>Tiền cước: {parseInt(item.Amount).toString().replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ','
                                    )}đ</Text>
                                    
                                </Col>
                                {/* <Col style={styles.rightBlock}> */}
                                <Checkbox value={state.data[key].isChecked} onValueChange={() => onGetBill(key)} color="#243A73" style={styles.checkBox} />
                                {/* </Col> */}
                            </Row>
                        </TouchableWithoutFeedback>
                    )
                    }
                    <Row style={styles.row}>
                        <Col>
                            <Text style={styles.total}>Tổng cước thanh toán: {parseInt(state.TotalAmount).toString().replace(
                                /\B(?=(\d{3})+(?!\d))/g,
                                ','
                            )}đ</Text>
                        </Col>
                    </Row>
                </Grid>
                <Button
                    full
                    disabled={disable.btnStatus}
                    style={styles.btn}
                    onPress={() => onPayBill()}>
                    <Text style={styles.txtBtn}>Thu Tiền</Text>
                </Button>
                {
                    disable.spinnerStatus ? (
                        <Spinner />
                    ) : null
                }
            </Content>
        </Container>
    );
}

const styles = StyleSheet.create({
    btn: {
        margin: 60,
        marginTop: 10,
        marginBottom: 20,
        padding: 20,
        backgroundColor: '#243A73',
        borderRadius: 5
    },
    title: {
        color: '#243A73',
        fontWeight: "700",
        // marginBottom: 12,
    },
    row: {
        margin: 16,
        borderRadius: 5,
        backgroundColor: 'rgb(255, 255, 255)',
        borderColor: 'rgb(0, 0, 0)',
        padding: 16,
        elevation: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
    },
    leftBlock: {
        width: Dimensions.get('window').width * 0.7,
        justifyContent: 'center',
    },
    rightBlock: {
        paddingLeft: 10,
    },
    viewAbsolute: {
        flex: 1,
        width: '100%',
        backgroundColor: 'transparent',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    checkBox: {
        borderWidth: 1,
        position: 'absolute',
        right: 10,
        top: 10
    },
    txtBtn: {
        fontSize: 16,
        fontWeight: '700'
    },
    total: {
        fontWeight: '700',
    }
});
