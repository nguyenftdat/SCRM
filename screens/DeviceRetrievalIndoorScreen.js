import React, { Component } from 'react';
import {
    Container,
    Content,
    Toast,
    Input,
    Form,
    Item,
    Picker,
    Icon,
    Text,
    Label,
    View,
    Button,
    Textarea,
    Thumbnail,
    Card,
    ListItem,
    CheckBox,
    Body
} from 'native-base';
import {
    TextInput,
    StyleSheet,
    FlatList,
    Dimensions,
    Alert,
    Image,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { DeviceRetrievalModel } from '../models/DeviceRetrievalModel';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';

const DeviceRetrievalIndoorScreen = ({ route, navigation }) => {
    const { user, setUser } = React.useContext(UserContext);
    const [state, setState] = React.useState({
        lyDoThuHoi: '',
        loaiThuHoi: '',
        ghiChu: '',
    });
    const [checked, setChecked] = React.useState(false);

    const toggleCheckbox = () => {
        setChecked((checked) => {
            return !checked;
        });
    };
    const { TBTB_ID, CN_ID, callback, type, data, schedule } = route.params;
    React.useEffect(() => {
        setState({
            ...state,
            lyDoThuHoi: global.listLich[0].LyDoThuHoi[0].Id,
            loaiThuHoi: 'RECOVER',
        });
    }, []);

    // tao gia tri luu tru Mac Wifi
    const storeOldMacWifi = async (value) => {
        try {
            await AsyncStorage.setItem(`@old_Wifi_${schedule.yc_id}`, value);
            let old_Wifi = await AsyncStorage.getItem(`@old_Wifi_${schedule.yc_id}`);
            let new_Wifi = await AsyncStorage.getItem(`@new_Wifi_${schedule.yc_id}`);
            if (new_Wifi && old_Wifi && (old_Wifi.toLowerCase() == new_Wifi.toLowerCase())) {
                await AsyncStorage.removeItem(`@new_Wifi_${schedule.yc_id}`);
            }
        } catch (e) {
            // saving error
        }
    };
    const onSubmit = () => {
        const payload: DeviceRetrievalModel = {
            TBTB_ID: TBTB_ID,
            LyDoThuHoi: state.lyDoThuHoi,
            TT_ID_ThuHoi: state.loaiThuHoi,
            CN_ID: CN_ID,
            GHI_CHU: state.ghiChu,
        };
        // console.log(payload)
        // navigation.navigate('SearchDeviceScreen');
        callAPI(ApiMethods.THUHOI, user.Token, payload, processSubmit, null);
    };
    const processSubmit = (data) => {
        Toast.show({
            text: 'Cập nhật thành công',
            duration: 2000,
            position: 'center',
            textStyle: { textAlign: 'center' },
        });
        callback();
        navigation.pop();
    };
    const onValueChange_LoaiThuHoi = (value: string) => {
        setState({ ...state, loaiThuHoi: value });
    };

    const onValueChange_LyDoThuHoi = (value: string) => {
        setState({ ...state, lyDoThuHoi: value });
    };

    const onSubmitIndoor = () => {
        const payload: DeviceRetrievalModel = {
            ID: data.ID,
            TaiSuDung: checked,
            LyDoThuHoi: state.lyDoThuHoi,
            GhiChu: state.ghiChu
        };
        storeOldMacWifi(data.MaSo1);
        callAPI(ApiMethods.THUHOI_THIETBI_KDM, user.Token, payload, processSubmit, null);
    }

    return (
        <Container>
            <Content padder>
                <Card style={{ padding: 8 }}>
                    <Grid>
                        <Row style={styles.row}>
                            <Col style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={styles.title}>Thông tin thiết bị</Text>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>ID:</Text>
                            </Col>
                            <Col>
                                <Text>{data.ID}</Text>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Mã số 1:</Text>
                            </Col>
                            <Col>
                                <Text>{data.MaSo1}</Text>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Ngày áp dụng: </Text>
                            </Col>
                            <Col>
                                <Text>{data.NgayApDung}</Text>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Ghi chú:</Text>
                            </Col>
                            <Col>
                                <Text>{data.GhiChu}</Text>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Trạng thái:</Text>
                            </Col>
                            <Col>
                                <Text>{data.TrangThaiId}</Text>
                            </Col>
                        </Row>
                    </Grid>
                </Card>
                <Card style={{ padding: 8 }}>
                    <Grid>
                        <Row style={styles.row}>
                            <Col>
                                <ListItem onPress={toggleCheckbox}>
                                    <CheckBox checked={checked} onPress={toggleCheckbox} />
                                    <Body>
                                        <Text style={{ fontStyle: 'italic' }}>Tái sử dụng</Text>
                                    </Body>
                                </ListItem>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Lý do thu hồi:</Text>
                            </Col>
                            <Col>
                                <Form>
                                    <Item Picker>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name="arrow-down" />}
                                            selectedValue={state.lyDoThuHoi}
                                            onValueChange={onValueChange_LyDoThuHoi}>
                                            {global.listLich[0].LyDoThuHoi.map((item, index) => {
                                                return (
                                                    <Picker.Item
                                                        label={item.Text}
                                                        value={item.Id}
                                                        key={index}
                                                    />
                                                );
                                            })}
                                        </Picker>
                                    </Item>
                                </Form>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Ghi chú:</Text>
                            </Col>
                            <Col>
                                <Textarea
                                    style={styles.noteBlock}
                                    value={state.ghiChu}
                                    placeholder="Nội dung ghi chú"
                                    rowSpan={2}
                                    onChangeText={(text) => {
                                        setState({ ...state, ghiChu: text });
                                    }}
                                />
                            </Col>
                        </Row>
                    </Grid>
                </Card>
                <Button full rounded style={styles.btn} onPress={onSubmitIndoor}>
                    <Text>Thu hồi thiết bị</Text>
                </Button>
            </Content>
        </Container>
    );
};
export default DeviceRetrievalIndoorScreen;
const styles = StyleSheet.create({
    row: {
        marginBottom: 10,
    },
    leftBlock: {
        justifyContent: 'center',
        width: Dimensions.get('window').width * 0.3,
        marginBottom: 10
    },
    btn: {
        marginTop: 10,
        margin: 40,
        marginBottom: 20
    },
    noteBlock: {
        marginLeft: 10,
        borderBottomWidth: 0.5,
        borderColor: 'rgba(0,0,0,0.2)'
    },
    title: {
        fontWeight: 'bold'
    }
});
