import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  Toast,
  Input,
  Form,
  Item,
  Picker,
  Icon,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  DatePicker,
  Right,
  Left,
  Body,
  Card,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  TouchableOpacity,
  Platform
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Grid, Col, Row } from 'react-native-easy-grid';
import SearchIcon from '../assets/img/search_icon.png';
import { formatDate } from '../utils/Utils';
import { CustomerReportMoney } from '../components/CustomerReportMoney';
import { CustomerReportMoneyModel } from '../models/CustomerReportMoneyModel';
import Moment from 'moment';

export default function SearchBillScreen({ route, navigation }) {
  const { user, setUser } = React.useContext(UserContext);
  const addDays = (date: Date, days: number) => {
    date.setDate(date.getDate() + days);
    return date;
  };
  const [state, setState] = React.useState({
    startDate: addDays(new Date, -7),
    endDate: new Date(),
    listData: [],
    tongTien: '',
  });
  const [enable, setEnable] = React.useState(false);

  const setStartDate = (startDate: Date) => {
    setState({ ...state, startDate: startDate });
  };
  const setEndDate = (endDate: Date) => {
    setState({ ...state, endDate: endDate });
  };

  const onSubmit = () => {
    loadData();
  };

  // report
  const onPressItem = (data: CustomerReportMoneyModel) => {
    navigation.push('CollectMoneyScreen', { data });
  };

  const loadData = () => {
    setEnable(false);
    const payload = {
      FromDate: Moment(state.startDate).format('YYYY-MM-DD') ,
      ToDate: Moment(state.endDate).format('YYYY-MM-DD'),
    };
    // console.log(payload);
    callAPI(
      ApiMethods.BAOCAO_THUCUOC_NHANVIEN_NEW,
      user.Token,
      payload,
      processSubmit,
      errorSubmit
    );
  };

  const processSubmit = (data) => {
    setState({
      ...state,
      listData: data[0],
      tongTien: data[0].TongTienDaThu.toString().replace(
        /\B(?=(\d{3})+(?!\d))/g,
        ','
      ),
    });
    setEnable(true);
  };
  const errorSubmit = (error) => {
    Toast.show({
      text: error.message,
      duration: 2000,
      position: 'center',
      textStyle: { textAlign: 'center' },
    });
    setState({
      ...state, 
      listData: {
        TongPhieuDaThu: 0
      },
      tongTien: 0
    })
  };
  React.useEffect(() => {
    loadData();
  }, []);
  React.useEffect(() => {
    loadData();
    // console.log(state.startDate + ' ' + state.endDate)
  }, [state.startDate, state.endDate])
  return (
    <Container>
      <Header searchBar rounded style={{ padding: 8 }}>
        <Item style={{ alignContent: 'center' }}>
          <Text style={{ marginLeft: 15 }}>Từ</Text>
          <DatePicker
            defaultDate={state.startDate}
            minimumDate={new Date(2000, 1, 1)}
            maximumDate={new Date(2999, 12, 31)}
            locale={'en-GB'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode={'default'}
            textStyle={{ color: 'green' }}
            placeHolderTextStyle={{ color: '#d3d3d3' }}
            onDateChange={setStartDate}
          />
          <Text style={{ marginLeft: 15 }}>Đến</Text>
          <DatePicker
            defaultDate={state.endDate}
            minimumDate={new Date(2000, 1, 1)}
            maximumDate={new Date(2999, 12, 31)}
            locale={'en-GB'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode={'default'}
            textStyle={{ color: 'green' }}
            placeHolderTextStyle={{ color: '#d3d3d3' }}
            onDateChange={setEndDate}
          />
          <Right style={{ marginRight: 8 }}>
            <TouchableOpacity style={styles.searchBtn} onPress={onSubmit}>
              <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-search`} />
            </TouchableOpacity>
          </Right>
        </Item>
      </Header>
      <Content padder>
        <Card style={styles.card}>
          <Text style={styles.dataText}>
            {Moment(state.startDate).format('DD/MM/YYYY')} -{' '}
            {Moment(state.endDate).format('DD/MM/YYYY')}
          </Text>
          <Text style={styles.fontText}>
            Tổng số KH thu: {state.listData.TongPhieuDaThu}
          </Text>
          <Text style={styles.fontText}>Tổng tiền: {state.tongTien}đ</Text>
        </Card>
        {enable ? (
          <CustomerReportMoney
            listCustomer={state.listData.ListPhieuDaThu}
            // onPressItem={onPressItem}
          />
        ) : null}
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  dataText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  card: {
    padding: 8,
    alignItems: 'center',
  },
  fontText: {
    fontStyle: 'italic'
  }
});
