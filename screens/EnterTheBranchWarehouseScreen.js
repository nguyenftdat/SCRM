import React, { Component } from 'react';
import {
  Container,
  Content,
  Toast,
  Input,
  Form,
  Item,
  Picker,
  Icon,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  Card,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
  TouchableOpacity,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import OpenList from '../components/OpenList';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { UserContext } from '../context/UserContext';
import EnterTheBranchWarehousePayload from '../models/EnterTheBranchWarehousePayload';
import MacCodeScanner from '../components/MacCodeScanner';

const EnterTheBranchWarehouseScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    loai: '',
    maSo: '',
    listMaSo: [],
  });
  const [disable, setDisable] = React.useState(true);
  const [data, setData] = React.useState(null);
  const openList = React.useRef(null);
  const macModem = React.useRef(null);
  React.useEffect(() => {
    setState({ ...state, loai: '1' });
  }, []);
  React.useEffect(() => {
    if (state.maSo != []) {
      setDisable(false);
    }
    else setDisable(true);
  }, [state.maSo])
  const onSubmit = () => {
    const payload: EnterTheBranchWarehousePayload = {
      Type: state.loai,
      Entry: state.maSo,
    };
    callAPI(ApiMethods.NHAPKHO, user.Token, payload, processNhapKho, null);
  };
  const processNhapKho = (data) => {
    Toast.show({
      text: 'Nhập về kho chi nhánh thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.navigate('EnterTheBranchWarehouseScreen');
  };
  const onValueChange_loai = (value: string) => {
    setState({ ...state, loai: value });
  };
  const addItemMaSo1 = (MS1: string) => {
    setState({ ...state, maSo: MS1 });
  };
  const onMaSo = () => {
    checkSearch();
  };
  const ktra = () => {
    if (data.length == 1) {
      // openList.current.onClose();
      setState({ ...state, maSo: data[0].Id });
    } else if (data.length > 1) {
      openList.current.showModal();
    } else {
      Toast.show({
        text: 'Khong tim thay ma so 1 ban yeu cau',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    }
  };

  // tìm kiếm
  const checkSearch = () => {
    if (state.maSo.length < 6) {
      Toast.show({
        text: 'Vui lòng bạn nhập mã số từ 6 ký tự để tìm kiếm',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      const payload: WarehouseManagementPayload = {
        Type: state.loai,
        Entry: state.maSo,
      };
      callAPI(ApiMethods.TIMTHIETBI, user.Token, payload, processTKMS, null);
    }
  };
  const processTKMS = (data) => {
    setData(data);
  };
  React.useEffect(() => {
    if (data !== null) {
      ktra();
    }
  }, [data]);
  const addMAC = (value) => {
    setState({ ...state, maSo: value })
  }
  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 10 }}>
          <Grid>
            <Row>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Loại:</Text>
              </Col>
              <Col>
                <Form>
                  <Item Picker>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.loai}
                      onValueChange={onValueChange_loai}>
                      <Picker.Item label="Thiết bị" value="1" />
                      <Picker.Item label="Smart card" value="2" />
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số:</Text>
              </Col>
              <Col>
                <Form style={styles.formStyle}>
                  <Item>
                    <Input
                      placeholder="Nhập mã số"
                      value={state.maSo}
                      onChangeText={(text) => {
                        setState({ ...state, maSo: text });
                      }}
                    />
                    <TouchableOpacity onPress={() => macModem.current.showModal()}>
                      <Icon name="crop-free" type='MaterialIcons' />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.searchBtn} onPress={onMaSo}>
                      <Icon name="search" type='MaterialIcons' />
                    </TouchableOpacity>
                  </Item>
                </Form>
              </Col>
            </Row>
          </Grid>
          <Button full rounded style={styles.btn} onPress={onSubmit} disabled={disable}>
            <Text>Nhập kho</Text>
          </Button>
        </Card>
      </Content>
      <OpenList
        ref={(el) => (openList.current = el)}
        datalist={data}
        addItemMaSo1={addItemMaSo1}
      />
      <MacCodeScanner
        ref={(el) => (macModem.current = el)}
        addMAC={addMAC}
      />
    </Container>
  );
};
export default EnterTheBranchWarehouseScreen;
const styles = StyleSheet.create({
  btn: {
    margin: 40,
    marginBottom: 20
  },
  searchBtn: {},
  textStyle: {
    fontWeight: 'bold',
  },
  formStyle: {
    // borderWidth: 1,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
});
