import React from 'react';
import { modal_styles } from '../styles/modal';
import {
    Modal,
    View,
    TouchableOpacity,
    StyleSheet,
    Vibration
} from 'react-native';
import {
    Container,
    Content,
    Icon,
    Text,
    Thumbnail
} from 'native-base';
import { BarCodeScanner } from 'expo-barcode-scanner';
const MacCodeScanner = React.forwardRef((props, ref) => {
    const intervalId = React.useRef(0);
    const [modalVisible, setModalVisible] = React.useState(false);
    const [hasPermission, setHasPermission] = React.useState(null);
    const [scanned, setScanned] = React.useState(false);
    const [status, setStatus] = React.useState(false);
    const [state, setState] = React.useState({
        barcode: '',
        cameraType: 'back',
        text: 'Scan Barcode',
        torchMode: 'off',
        type: '',
    });
    const barcodeReceived = (e) => {
        setScanned(true);
        if (e.data !== state.barcode || e.type !== state.type) Vibration.vibrate();
        setState({
            ...state,
            barcode: e.data,
            //text: `${e.data} (${e.type})`,
            text: `${e.data}`,
            type: e.type,
        });
        setStatus(true);
        props.addMAC(e.data);
        setModalVisible(!modalVisible);
    };
    React.useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
        intervalId.current = setInterval(() => {
            setScanned(false);
            setStatus(false);
        }, 5000);
        return function cleanup() {
            clearInterval(intervalId.current);
        };
    }, [])
    React.useImperativeHandle(ref, () => ({
        showModal() {
            setModalVisible(true);
        }
    }));
    if (hasPermission === null) {
        return <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text>Requesting for camera permission</Text></View>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
                setModalVisible(!modalVisible);
            }}>
            <Container>
                <View style={modal_styles.centeredView}>
                    <Content>
                        <View style={modal_styles.mainModalView}>
                            <TouchableOpacity
                                style={modal_styles.closeBtn}
                                onPress={() => {
                                    setModalVisible(!modalVisible);
                                }}>
                                <Icon name="close" />
                            </TouchableOpacity>
                            <BarCodeScanner
                                onBarCodeScanned={scanned ? undefined : barcodeReceived}
                                style={[StyleSheet.absoluteFillObject, modal_styles.container]}>
                                <Thumbnail
                                    large
                                    square
                                    style={modal_styles.qr}
                                    source={require('../assets/img/qr_scanner.png')}
                                />
                                <Text onPress={() => setModalVisible(!modalVisible)} style={modal_styles.cancel}>
                                    Cancel
                                </Text>
                            </BarCodeScanner>
                        </View>
                    </Content>
                </View>
            </Container>
        </Modal>
    );
});
export default MacCodeScanner;
