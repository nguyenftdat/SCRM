//React import here
import * as React from 'react';
//External imports here
import {
  Container,
  Content,
  Text,
  View,
  Button,
  Body
} from 'native-base';
import { ScrollView, Alert } from 'react-native';
import CardGroupThietBi from '../components/CardGroupThietBi';
import { GroupThietBi, ThietBiRow } from '../models/GroupThietBi';
import ModalAddThietBi from '../components/ModalAddThietBi';
import ModalAddIndoorEquip from '../components/ModalAddIndoorEquip';
import MultiSelect from 'react-native-multiple-select';
import AsyncStorage from '@react-native-async-storage/async-storage';

const MaintenanceScreen = ({ route, navigation }) => {
  const schedule = route.params.schedule;
  //type of schedule
  const type = route.params.type || null;
  let groups: Array<GroupThietBi> = route.params.groups || [{
    SO_HOPDONG: "",
    MaThueBao: schedule.MaThueBao || schedule.TB_MATHUEBAO,
    ListThietBi: []
  }];
  const [state, setState] = React.useState({
    groups,
  });
  const modalAddThietBi = React.useRef(null);
  const showModalAddThietBi = (index: number) => () => {
    modalAddThietBi.current.showModal(index);
  }
  const removeGroup = (index: number) => () => {
    let groups: Array<GroupThietBi> = [...state.groups];
    groups.splice(index, 1);
    setState({ groups });
  }

  const addItem = (index: number, item: ThietBiRow) => {
    console.log(item);
    let groups: Array<GroupThietBi> = [...state.groups];
    groups[index].ListThietBi.push(item);
    setState({ groups });
  }
  const removeItem = (groupIdx: number) => (index: number) => () => {
    let groups: Array<GroupThietBi> = [...state.groups];
    groups[groupIdx].ListThietBi.splice(index, 1);
    setState({ groups });
  }
  const onSaveData = () => {
    storeData(state.groups);
    if (state.groups[0].ListThietBi.length === 0) {
      Alert.alert(
        'Thông báo',
        'Hiện tại bạn chưa nhập thiết bị bạn có chắc chắn bỏ qua không.',
        [
          { text: 'YES', onPress: () => navigation.navigate('FinishScheduleModal', { schedule: schedule, thietBiSuDung: state.groups, type: type }) },
          { text: 'NO', onPress: () => console.log('Cancel Pressed') },
        ],
        // { cancelable: true }  
      );
    }
    else {
      navigation.navigate('FinishScheduleModal', { schedule: schedule, thietBiSuDung: state.groups, type: type });
    }
  }
  const storeData = async (data) => {
    try {
      await AsyncStorage.setItem(`@equip_${schedule.yc_id}`, JSON.stringify(data));
    } catch (error) {
      // Error saving data
    }
  };
  // const getData = async (id) => {
  //   try {
  //     const value = await AsyncStorage.getItem(`@equip_${id}`);
  //     if (value !== null) {
  //       // We have data!!
  //       return value;
  //     }
  //   } catch (error) {
  //     // Error retrieving data
  //   }
  // };
  return (
    <Container>
      <Content padder>
        {state.groups.map((item, index) => (
          <CardGroupThietBi group={item} key={index}
            showModal={showModalAddThietBi(index)}
            removeGroup={removeGroup(index)}
            removeItem={removeItem(index)} />
        ))}
        {
          type === 'indoor' ?
            (<ModalAddIndoorEquip ref={(el) => modalAddThietBi.current = el} addItem={addItem} />) :
            (<ModalAddThietBi ref={(el) => modalAddThietBi.current = el} addItem={addItem} />)
        }
        <Button full rounded style={{ margin: 40, marginBottom: 10, marginTop: 10 }}
          onPress={onSaveData}>
          <Text>Lưu</Text>
        </Button>
      </Content>
    </Container>

  );
};

export default MaintenanceScreen;
