import React, { Component, useState } from 'react';
import {
  Container,
  Content,
  Header,
  View,
  Input,
  Text,
  Button,
  Icon,
  Item,
  Textarea,
  Thumbnail,
  Card,
  Form,
  Picker,
  Toast,
  Spinner
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { StyleSheet, TouchableOpacity, Platform, Dimensions, Alert } from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import CustomerSearchPayload from '../models/CustomerSearchPayload';
import SearchIcon from '../assets/img/search_icon.png';
import { CustomerList } from '../components/CustomerList';
import { setApiKey } from 'expo-location';
import OpenList from '../components/OpenList';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MacCodeScanner from '../components/MacCodeScanner';

const ClientLookupScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const { TBTB_ID, KH_ID, CN_ID, callback, schedule } = route.params;
  const [state, setState] = React.useState({
    ndxuly: '',
    soHuu: '',
    MS1: ''
  });
  const [status, setStatus] = React.useState(false);
  const [data_1, setData_1] = React.useState({
    listDataMS: null,
  });

  // tao gia tri luu tru Mac Wifi
  const storeNewMacWifi = async (value) => {
    try {
      await AsyncStorage.setItem(`@new_Wifi_${schedule.yc_id}`, value);
      let old_Wifi = await AsyncStorage.getItem(`@old_Wifi_${schedule.yc_id}`);
      let new_Wifi = await AsyncStorage.getItem(`@new_Wifi_${schedule.yc_id}`);
      if (new_Wifi && old_Wifi && (old_Wifi.toLowerCase() == new_Wifi.toLowerCase())) {
        await AsyncStorage.removeItem(`@old_Wifi_${schedule.yc_id}`);
      }
    } catch (e) {
      // saving error
    }
  };

  const openListMS1 = React.useRef(null);
  const macModem = React.useRef(null);
  React.useEffect(() => {
    resetState();
  }, []);

  const resetState = () => {
    setState({
      ...state,
      soHuu: global.listLich[0].TrangThaiSoHuu[0].Id
    });
  }

  const onValueChange_soHuu = (value: string) => {
    setState({ ...state, soHuu: value });
  };

  // TÌM KIẾM MÃ SỐ 1
  const addItemMaSo1 = (MS1: string) => {
    setState({ ...state, MS1: MS1 });
  };
  const updateMacWifi = () => {
    if (state.MS1.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 1 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: state.MS1,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_KDM,
        user.Token,
        payload,
        processMacWifi,
        null
      );
    }
  };
  const processMacWifi = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'bottom',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setData_1({ ...data_1, listDataMS: data });
    }
  };
  const ktra1 = () => {
    if (data_1.listDataMS.length == 1) {
      setState({ ...state, MS1: data_1.listDataMS[0].Id });
    } else if (data_1.listDataMS.length > 1) {
      openListMS1.current.showModal();
    } else {
      Alert.alert('Khong tim thay ma so 1 ban yeu cau');
    }
  };

  React.useEffect(() => {
    if (data_1.listDataMS !== null) {
      ktra1();
    }
  }, [data_1]);


  const onSubmit = () => {
    setStatus(true);
    const payload = {
      TB_MATHUEBAO: TBTB_ID,
      KH_ID: KH_ID,
      CN_ID: CN_ID,
      MaSo1: state.MS1,
      TrangThaiSoHuu: state.soHuu,
      GhiChu: state.ndxuly
    };
    // console.log(payload);
    storeNewMacWifi(state.MS1);
    callAPI(ApiMethods.LAPMOI_THIETBI_KDM, user.Token, payload, processSubmit, errorSubmit);
  }
  const processSubmit = () => {
    setStatus(false);
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    callback();
    navigation.pop();
  }
  const errorSubmit = (error) => {
    setStatus(false);
    alert(error.message);
  }

  const addMAC = (value) => {
    setState({ ...state, MS1: value })
  }
  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={{ alignItems: 'center' }}>
                <Text style={styles.title}>Thông tin thiết bị</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Mã số 1</Text>
              </Col>
              <Col>
                <Form>
                  <Item style={{ marginLeft: 0 }}>
                    <Input
                      placeholder="Nhập mã số"
                      value={state.MS1}
                      onChangeText={(text) => {
                        setState({ ...state, MS1: text });
                      }}
                    />
                    <TouchableOpacity onPress={() => macModem.current.showModal()}>
                      <Icon name="crop-free" type='MaterialIcons' />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.searchBtn} onPress={updateMacWifi}>
                      <Icon
                        name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                    </TouchableOpacity>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Sở hữu</Text>
              </Col>
              <Col>
                <Form style={styles.formStyle}>
                  <Item Picker style={{ marginLeft: 0, paddingLeft: 10 }}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.soHuu}
                      onValueChange={onValueChange_soHuu}>
                      {global.listLich[0].TrangThaiSoHuu.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Ghi chú</Text>
              </Col>
              <Col>
                <Textarea
                  style={styles.inputStyle}
                  rowSpan={2}
                  onChangeText={(text) => {
                    setState({ ...state, ndxuly: text });
                  }}
                />
              </Col>
            </Row>
          </Grid>
          <Button
            full
            rounded
            style={{ margin: 40, marginTop: 20, marginBottom: 10 }}
            disabled={status}
            onPress={onSubmit}>
            <Text>Lưu</Text>
          </Button>
        </Card>
      </Content>
      {status ? (
        <Spinner />
      ) : null}
      <OpenList
        ref={(el) => (openListMS1.current = el)}
        datalist={data_1.listDataMS}
        addItemMaSo1={addItemMaSo1}
      />
      <MacCodeScanner
        ref={(el) => (macModem.current = el)}
        addMAC={addMAC}
      />
    </Container>
  );
};
export default ClientLookupScreen;
const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold'
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.35,
    justifyContent: 'center',
  },
  row: {
    marginTop: 10,
  },
  inputStyle: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.4)',
  },
});
