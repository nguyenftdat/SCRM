//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import CustomerReportMoneyModel from '../models/CustomerReportMoneyModel';
import Moment from 'moment';

type Props = {
  item: CustomerReportMoneyModel,
  index: number,
  onPressItem: (customer: CustomerReportMoneyModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#3A5BA0' : '#fff';
  return (
    // <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
    <Card
      style={[styles.cardBox, {
        backgroundColor: backgroundColor,
      }]}>
      {
        props.index % 2 == 0 ? (
          <View>
            <Text style={styles.txt_even}><Text style={styles.title_even}>Ngày thu: </Text>{Moment(props.item.TransDate).format('DD-MM-YYYY HH:mm:ss')}</Text>
            <Text style={styles.txt_even}><Text style={styles.title_even}>Tên khách hàng: </Text>{props.item.HoTen}</Text>
            <Text style={styles.txt_even}><Text style={styles.title_even}>Địa chỉ: </Text>{props.item.DiaChi}</Text>
            <Text style={styles.txt_even}><Text style={styles.title_even}>Tổng tiền: </Text>
              {props.item.TotalAmount.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g,
                ','
              )}đ
            </Text>
            <Text style={styles.txt_even}><Text style={styles.title_even}>Mã khách hàng: </Text>{props.item.CustCode}</Text>

          </View>) : (
          <View>
            <Text style={styles.txt_odd}><Text style={styles.title_odd}>Ngày thu: </Text>{Moment(props.item.TransDate).format('DD-MM-YYYY HH:mm:ss')}</Text>
            <Text style={styles.txt_odd}><Text style={styles.title_odd}>Tên khách hàng: </Text>{props.item.HoTen}</Text>
            <Text style={styles.txt_odd}><Text style={styles.title_odd}>Địa chỉ: </Text>{props.item.DiaChi}</Text>
            <Text style={styles.txt_odd}><Text style={styles.title_odd}>Tổng tiền: </Text>
              {props.item.TotalAmount.toString().replace(
                /\B(?=(\d{3})+(?!\d))/g,
                ','
              )}đ
            </Text>
            <Text style={styles.txt_odd}><Text style={styles.title_odd}>Mã khách hàng: </Text>{props.item.CustCode}</Text>
          </View>
        )
      }
    </Card>
    // </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<CustomerReportMoneyModel>,
  onPressItem: (customer: CustomerReportMoneyModel) => void,
};
export const CustomerReportMoney = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return (
          <CustomerListItem
            item={item}
            index={index}
            onPressItem={props.onPressItem}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
const styles = StyleSheet.create({
  title_even: {
    fontWeight: '700',
    color: '#fff'
  },
  title_odd: {
    fontWeight: '700',
  },
  txt_even: {
    color: '#fff',
    // fontStyle: 'italic'
  },
  txt_odd: {
    // fontStyle: 'italic'
  },
  cardBox: {
    padding: 12,
    marginBottom: 12
  }
})