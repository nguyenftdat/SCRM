import React from "react";
import {
    Card,
    Text,
    Form,
    Item,
    Picker,
    Icon,
    Input
} from 'native-base';
import {
    TouchableOpacity
} from 'react-native';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { stylesLib } from '../styles/stylesLib';
const EquipmentList = () => {
    const [state, setState] = React.useState({
        selectedKeyWifi: global.DanhMucThietBiWifi[0].Id,
        macWifiM: '',
        macWifiC: '',
        macModemM: '',
        macModemC: '',
        wifiStt: 'M',
        modemStt: 'M',
    });
    const newMacWifi = React.useRef(null);
    const oldMacWifi = React.useRef(null);
    const newMacModem = React.useRef(null);
    const oldMacModem = React.useRef(null);
    const onValueChangeKeyWifi = (value: string) => {
        setState({ ...state, selectedKeyWifi: value });
    };
    return (
        <Card style={{ padding: 8 }}>
            <Grid>
                <Row>
                    <Col style={stylesLib.ct}><Text style={stylesLib.b_txt}>THIẾT BỊ VẬT TƯ</Text></Col>
                </Row>
                {/* <Row>
                    <Col>
                        <ListItem onPress={toggleCheckbox}>
                            <CheckBox checked={checked} onPress={toggleCheckbox} />
                            <Body>
                                <Text style={{ fontStyle: 'italic' }}>Khách hàng có đóng trước</Text>
                            </Body>
                        </ListItem>
                    </Col>
                </Row> */}
                <Row>
                    <Col style={stylesLib.l_block_w_35}>
                        <Text style={stylesLib.b_txt}>Thay Wifi</Text>
                    </Col>
                    <Col>
                        <Form style={{ borderBottomColor: "rgba(0, 0, 0, 0.1)", borderBottomWidth: 1 }}>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={state.selectedKeyWifi}
                                onValueChange={onValueChangeKeyWifi}
                            >
                                {
                                    global.DanhMucThietBiWifi.map((item, index) => (
                                        <Picker.Item label={item.Text} value={item.Id} />
                                    ))
                                }
                            </Picker>
                        </Form>
                    </Col>
                </Row>
                <Row style={stylesLib.mt_10}>
                    <Col style={stylesLib.l_block_w_35}>
                        <Text style={stylesLib.b_txt}>MAC Wifi cũ</Text>
                    </Col>
                    <Col>
                        <Form>
                            <Item style={{ marginLeft: 0 }}>
                                <Input
                                    placeholder="Nhập mã số"
                                    value={state.macWifiC}
                                    onChangeText={(text) => {
                                        setState({ ...state, macWifiC: text, wifiStt: 'C' });
                                    }}
                                />
                                <TouchableOpacity onPress={() => oldMacWifi.current.showModal()}>
                                    <Icon name="crop-free" type='MaterialIcons' />
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={stylesLib.searchBtn} onPress={updateMacWifiC}>
                                    <Icon
                                        name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                                </TouchableOpacity> */}
                            </Item>
                        </Form>
                    </Col>
                </Row>
                <Row style={stylesLib.mt_10}>
                    <Col style={stylesLib.l_block_w_35}>
                        <Text style={stylesLib.b_txt}>MAC Wifi mới</Text>
                    </Col>
                    <Col>
                        <Form>
                            <Item style={{ marginLeft: 0 }} disabled>
                                <Input
                                    disabled={state.selectedKeyWifi == "-1" ? true : false}
                                    placeholder={state.selectedKeyWifi == "-1" ? "Không thay đổi" : "Nhập mã số"}
                                    value={state.macWifiM}
                                    onChangeText={(text) => {
                                        setState({ ...state, macWifiM: text, wifiStt: 'M' });
                                    }}
                                />
                                {state.selectedKeyWifi != "-1" ? (
                                    <TouchableOpacity onPress={() => newMacWifi.current.showModal()}>
                                        <Icon name="crop-free" type='MaterialIcons' />
                                    </TouchableOpacity>
                                ) : null}
                                {/* {state.selectedKeyWifi != "-1" ? (
                                    <TouchableOpacity style={stylesLib.searchBtn} onPress={updateMacWifiM}>
                                        <Icon
                                            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                                    </TouchableOpacity>
                                ) : null} */}
                            </Item>
                        </Form>
                    </Col>
                </Row>
                <Row style={stylesLib.mt_10}>
                    <Col style={stylesLib.l_block_w_35}>
                        <Text style={stylesLib.b_txt}>MAC modem cũ</Text>
                    </Col>
                    <Col>
                        <Form>
                            <Item style={{ marginLeft: 0 }}>
                                <Input
                                    placeholder="Nhập mã số"
                                    value={state.macModemC}
                                    onChangeText={(text) => {
                                        setState({ ...state, macModemC: text, modemStt: 'C' });
                                    }}
                                />
                                <TouchableOpacity onPress={() => oldMacModem.current.showModal()}>
                                    <Icon name="crop-free" type='MaterialIcons' />
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={stylesLib.searchBtn} onPress={updateMacModemC}>
                                    <Icon
                                        name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                                </TouchableOpacity> */}
                            </Item>
                        </Form>
                    </Col>
                </Row>
                <Row style={stylesLib.mt_10}>
                    <Col style={stylesLib.l_block_w_35}>
                        <Text style={stylesLib.b_txt}>MAC modem mới</Text>
                    </Col>
                    <Col>
                        <Form>
                            <Item style={{ marginLeft: 0 }}>
                                <Input
                                    placeholder="Nhập mã số"
                                    value={state.macModemM}
                                    onChangeText={(text) => {
                                        setState({ ...state, macModemM: text, modemStt: 'M' });
                                    }}
                                />
                                <TouchableOpacity onPress={() => newMacModem.current.showModal()}>
                                    <Icon name="crop-free" type='MaterialIcons' />
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={stylesLib.searchBtn} onPress={updateMacModemM}>
                                    <Icon
                                        name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                                </TouchableOpacity> */}
                            </Item>
                        </Form>
                    </Col>
                </Row>
            </Grid>
        </Card>
    );
}
export default EquipmentList;