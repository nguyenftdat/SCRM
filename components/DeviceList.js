//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import DeviceModel from '../models/DeviceModel';

type Props = {
  item: DeviceModel,
  index: number,
  onPressItem: (customer: DeviceModele) => void,
};

const CustomerListItem_MS1 = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          backgroundColor: backgroundColor,
          padding: 8,
          marginBottom: 10,
        }}>
        <Text>Mã số 1: {props.item.MaSo1}</Text>
        <Text>{props.item.CN_TEN_CHI_NHANH}</Text>
        <Text>Trạng thái: {props.item.TRANG_THAI}</Text>
        <Text>Bộ giải mã: {props.item.BOGIAIMA}</Text>
      </Card>
    </TouchableOpacity>
  );
};
const CustomerListItem_MS2 = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#e9edf4';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          backgroundColor: backgroundColor,
          padding: 8,
          marginBottom: 10,
        }}>
        <Text>Mã số 2: {props.item.MaSo2}</Text>
        <Text>{props.item.CN_TEN_CHI_NHANH}</Text>
        <Text>Trạng thái: {props.item.TRANG_THAI}</Text>
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<DeviceModel>,
  onPressItem: (customer: DeviceModel) => void,
};
export const DeviceList = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        if (item.MaSo1 != null) {
          return (
            <CustomerListItem_MS1
              item={item}
              index={index}
              onPressItem={props.onPressItem}
            />
          );
        } else {
          return (
            <CustomerListItem_MS2
              item={item}
              index={index}
              onPressItem={props.onPressItem}
            />
          );
        }
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
