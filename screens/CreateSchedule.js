import React, { useState, useEffect } from 'react';
import { Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import {
    Container,
    Content,
    Input,
    InputGroup,
    Button,
    Card,
    Form,
    DatePicker,
    Picker,
    Item,
    Icon,
    Toast
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import Moment from 'moment';
export default function CreateSchedule({ route, navigation }) {
    const { user, setUser } = React.useContext(UserContext);
    const { customer } = route.params;
    const [state, setState] = useState({
        ho_ten: customer.TB_HOTEN,
        dien_thoai: customer.TB_DIENTHOAI,
        ma_loai_yeucau: global.listLich[0].LoaiYeuCau[0].Id,
        ma_dichvu: global.listLich[0].DichVu[0].Id,
        do_uutien: global.listLich[0].DoUuTien[0].Id,
        ngay_hen: new Date,
        ghi_chu: '',
    });
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const showDateTimePicker = () => setDatePickerVisibility(true);
    const hideDateTimePicker = () => setDatePickerVisibility(false);
    const handleDatePicked = (date) => {
        setState({ ...state, ngay_hen: date });
        hideDateTimePicker();
    };

    const onChangeLoaiYC = (value) => {
        setState({ ...state, ma_loai_yeucau: value })
    }
    const onChangeDichVu = (value) => {
        setState({ ...state, ma_dichvu: value })
    }
    const onChangeDoUuTien = (value) => {
        setState({ ...state, do_uutien: value })
    }
    useEffect(() => {
        // defaultState();
        // console.log(global.listLich[0].LoaiYeuCau);
    }, [])
    const onSave = () => {
        // console.log(state.ngay_hen > new Date());
        if (state.ho_ten == '') {
            Toast.show({
                text: 'Vui lòng nhập họ tên.',
                duration: 3000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (state.dien_thoai == '') {
            Toast.show({
                text: 'Vui lòng nhập số điện thoại.',
                duration: 3000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (state.ma_loai_yeucau === '-1') {
            Toast.show({
                text: 'Vui lòng chọn loại yêu cầu.',
                duration: 3000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (state.ma_dichvu === '-1') {
            Toast.show({
                text: 'Vui lòng chọn loại dịch vụ.',
                duration: 3000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (state.ngay_hen < new Date()) {
            Toast.show({
                text: 'Vui lòng chọn thời gian hẹn diễn ra sau thời gian hiện tại.',
                duration: 3000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else {
            let payload = {
                CN_ID: customer.CN_ID,
                MaThueBao: customer.TB_MATHUEBAO,
                ho_ten: state.ho_ten,
                dien_thoai: state.dien_thoai,
                ma_loai_yeucau: state.ma_loai_yeucau,
                ma_dichvu: state.ma_dichvu,
                ngay_hen: Moment(state.ngay_hen).format('YYYY-MM-DD HH:mm:ss'),
                ghi_chu: state.ghi_chu,
                do_uutien: Number(state.do_uutien)
            }
            // console.log(payload);
            callAPI(
                ApiMethods.TAO_LICH,
                user.Token,
                payload,
                processData,
                null
            )
        }
    }
    const processData = () => {
        Toast.show({
            text: 'Cập nhật thành công',
            duration: 2000,
            position: 'center',
            textStyle: { textAlign: 'center' },
        });
        navigation.pop();
    }

    return (
        <Container>
            <Content padder>
                <Card style={{ padding: 8 }}>
                    <Grid>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Họ và tên:</Text>
                            </Col>
                            <Col>
                                <InputGroup>
                                    <Input
                                        w={32}
                                        placeholder="Nhập họ và tên"
                                        value={state.ho_ten}
                                        onChangeText={(text) => {
                                            setState({ ...state, ho_ten: text })
                                        }}
                                    />
                                </InputGroup>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Số điện thoại:</Text>
                            </Col>
                            <Col>
                                <InputGroup>
                                    <Input
                                        w={32}
                                        placeholder="Nhập số điện thoại"
                                        keyboardType="numeric"
                                        value={state.dien_thoai}
                                        onChangeText={(text) => {
                                            setState({ ...state, dien_thoai: text })
                                        }} />
                                </InputGroup>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Loại yêu cầu:</Text>
                            </Col>
                            <Col>
                                <Form>
                                    <Item Picker>
                                        <Picker
                                            mode="dropdown"
                                            enabled={true}
                                            iosIcon={<Icon name="arrow-down" />}
                                            selectedValue={state.ma_loai_yeucau}
                                            onValueChange={onChangeLoaiYC}>
                                            {global.listLich[0].LoaiYeuCau.map((item, index) => {
                                                return (
                                                    <Picker.Item
                                                        label={item.Text}
                                                        value={item.Id}
                                                        key={index} />
                                                )
                                            })}
                                        </Picker>
                                    </Item>
                                </Form>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Mức độ:</Text>
                            </Col>
                            <Col>
                                <Form>
                                    <Item Picker>
                                        <Picker
                                            mode="dropdown"
                                            enabled={true}
                                            iosIcon={<Icon name="arrow-down" />}
                                            selectedValue={state.do_uutien}
                                            onValueChange={onChangeDoUuTien}>
                                            {global.listLich[0].DoUuTien.map((item, index) => {
                                                return (
                                                    <Picker.Item
                                                        label={item.Text}
                                                        value={item.Id}
                                                        key={index} />
                                                )
                                            })}
                                        </Picker>
                                    </Item>
                                </Form>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Dịch vụ:</Text>
                            </Col>
                            <Col>
                                <Form>
                                    <Item Picker>
                                        <Picker
                                            mode="dropdown"
                                            enabled={true}
                                            iosIcon={<Icon name="arrow-down" />}
                                            selectedValue={state.ma_dichvu}
                                            onValueChange={onChangeDichVu}>
                                            {global.listLich[0].DichVu.map((item, index) => {
                                                return (
                                                    <Picker.Item
                                                        label={item.Text}
                                                        value={item.Id}
                                                        key={index} />
                                                )
                                            })}
                                        </Picker>
                                    </Item>
                                </Form>
                            </Col>
                        </Row>
                        <Row style={[styles.row, { marginTop: 10 }]}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Ngày hẹn:</Text>
                            </Col>
                            <Col style={{ paddingLeft: 10 }}>
                                <DateTimePicker
                                    mode="datetime"
                                    date={state.ngay_hen}
                                    is24Hour
                                    isVisible={isDatePickerVisible}
                                    onConfirm={handleDatePicked}
                                    onCancel={hideDateTimePicker}
                                />
                                <TouchableOpacity onPress={showDateTimePicker}>
                                    <Text>{Moment(state.ngay_hen).format('YYYY-MM-DD HH:mm:ss')}</Text>
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row style={styles.row}>
                            <Col style={styles.leftBlock}>
                                <Text style={styles.title}>Ghi chú:</Text>
                            </Col>
                            <Col style={{ justifyContent: 'center', }}>
                                <InputGroup>
                                    <Input placeholder="Nhập nội dung ghi chú" onChangeText={(text) => { setState({ ...state, ghi_chu: text }) }} />
                                </InputGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button full rounded light primary
                                    style={styles.createBtn}
                                    onPress={onSave}>
                                    <Text style={styles.txt}>TẠO LỊCH</Text>
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </Card>
            </Content>
        </Container>
    );
}
const styles = StyleSheet.create({
    row: {
        marginBottom: 10
    },
    leftBlock: {
        width: Dimensions.get('window').width * 0.35,
        justifyContent: 'center',
        // alignItems: 'center'
    },
    title: {
        fontSize: 16,
        fontWeight: '700'
    },
    txt: {
        color: '#fff',
        fontWeight: '700'
    },
    createBtn: {
        margin: 30
    }
})