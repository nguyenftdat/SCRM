//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import TimKiemXuatKhoResultListItemModel from '../models/TimKiemXuatKhoResultListItemModel';

type Props = {
  item: TimKiemXuatKhoResultListItemModel,
  index: number,
  onPressItem: (customer: TimKiemXuatKhoResultListItemModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          backgroundColor: backgroundColor,
          padding: 8,
          marginBottom: 10,
        }}>
        <Text>{props.item.MaSo1}</Text>
        {props.item.MaSo2 ? <Text>{props.item.MaSo2}</Text> : null}
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<TimKiemXuatKhoResultListItemModel>,
  onPressItem: (customer: TimKiemXuatKhoResultListItemModel) => void,
};
export const ListWarehouseExportCancel = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return (
          <CustomerListItem
            item={item}
            index={index}
            onPressItem={props.onPressItem}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
