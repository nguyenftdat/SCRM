//@flow
export type TimKiemXuatKhoResultListItemModel = {
  ID_XUATKHO_KTS: number,
  NGAY_XUAT: Date,
  MaSo1: string,
  MaSo2: string,
  SERIAL: string,
  TRANG_THAI: string,
};
