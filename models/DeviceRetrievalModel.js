export type DeviceRetrievalModel = {
  TBTB_ID: number,
  LyDoThuHoi: string,
  TT_ID_ThuHoi: string,
  CN_ID: number,
  GHI_CHU: string,
};
