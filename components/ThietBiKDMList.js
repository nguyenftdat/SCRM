//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import ListThietBiModel from '../models/ListThietBiModel';

type Props = {
  item: ListThietBiModel,
  index: number,
  onPressItem: (TTTB: ListThietBiModel) => void,
  callback: () => void
};

const ThietBiKDM = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          backgroundColor: backgroundColor,
          padding: 8,
        }}>
        <Text>
          ->. {props.item.TB_ID} - {props.item.ID}
        </Text>
        <Text style={{ paddingLeft: 15 }}>Mã số 1: {props.item.MaSo1}</Text>
        <Text style={{ paddingLeft: 15 }}>
          Ngày áp dụng: {props.item.NgayApDung}
        </Text>
        <Text style={{ paddingLeft: 15 }}>
          Trạng thái: {props.item.TrangThaiId}
        </Text>
        <Text style={{ paddingLeft: 15 }}>Ghi chú: {props.item.GhiChu}</Text>
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listTB: Array<ListThietBiModel>,
  onPressItem: (TTTB: ListThietBiModel) => void,
  callback: () => void
};
export const ThietBiKDMList = (props: CustomerListProps) => {
  return (
    <FlatList
      style={{padding: 8}}
      data={props.listTB}
      renderItem={({ item, index }) => {
        return (
          <ThietBiKDM
            item={item}
            index={index}
            onPressItem={props.onPressItem}
            callback={props.callback}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};