//@flow
export type TimKiemXuatKhoModel = {
  MaSo1: string,
  MaSo2: string,
  StartDate: Date,
  EndDate: Date,
  NV_NHAN: string,
  Type: string,
};
