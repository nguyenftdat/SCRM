//@flow
import React from 'react';
import { Text, View, Form } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import ListCuocModel from '../models/ListCuocModel';

type Props = {
  item: ListCuocModel,
  index: number,
  onPressItem: (customer: ListCuocModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#e9edf4';
  return (
    // <TouchableOpacity onPress={()=>props.onPressItem(props.item)}>
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: backgroundColor,
      }}>
      <View style={{ flex: 1, borderWidth: 1, borderColor: 'white' }}>
        <Text style={{ paddingHorizontal: 10 }}>
          {props.item.RowNumber}. Tháng {props.item.T_TEN_THANG},{' '}
          {props.item.TEN_DICHVU} ({props.item.TB_MATHUEBAO})
        </Text>
        <Text style={{ paddingHorizontal: 10, paddingLeft: 15 }}>
          Tiền cước: {props.item.TONG_NO_TEXT}
        </Text>
        <Text style={{ paddingHorizontal: 10, paddingLeft: 15 }}>
          Tiền đã đóng: {props.item.TONG_DATHU_TEXT}
        </Text>
        <Text style={{ paddingHorizontal: 10, paddingLeft: 15 }}>
          Tiền còn nợ: {props.item.TONG_CHUATHU_TEXT}
        </Text>
      </View>
    </View>
    // </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<ListCuocModel>,
  // onPressItem:(customer:ListThueBaoModel)=>void
};
export const CustomerCuocList = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return <CustomerListItem item={item} index={index} />;
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
