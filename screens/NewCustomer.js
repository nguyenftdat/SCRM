import {
    Container,
    Label,
    Content,
    Accordion,
    Icon,
    Input,
    Item,
    Form,
    Text
} from 'native-base';
import React, { useState } from 'react';
import {
    Modal,
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import Autocomplete from 'react-native-autocomplete-input';
import { modal_styles } from '../styles/modal';
import { stylesLib } from '../styles/stylesLib';
import { lowerCase, zip } from 'lodash';

const NewCustomer = () => {
    const testData = [
        {
            id: 1,
            addr: 'phường Tân Định, quận 1'
        },
        {
            id: 2,
            addr: 'phường Thạnh Mỹ Lợi, quận 2'
        },
        {
            id: 3,
            addr: 'phường Long Trường, quận 9'
        },
        {
            id: 4,
            addr: 'xã Thanh Bình, huyện Chợ Gạo'
        },
        {
            id: 5,
            addr: 'phường Linh Trung, TP. Thủ Đức'
        },
        {
            id: 6,
            addr: 'phường Linh Xuân, TP. Thủ Đức'
        },
        {
            id: 7,
            addr: 'phường 4, quận Bình Thạnh'
        },
    ];
    const testStreetData = [
        {
            id: 1,
            addr: 'đường VVN'
        },
        {
            id: 2,
            addr: 'đường NDT'
        },
        {
            id: 3,
            addr: 'đường TQK'
        },
        {
            id: 4,
            addr: 'đường VCK'
        },
        {
            id: 5,
            addr: 'đường HHT'
        },
        {
            id: 6,
            addr: 'đường PVĐ'
        },
        {
            id: 7,
            addr: 'đường TKTQ'
        },
    ];
    const [modalVisible, setModalVisible] = useState(false);
    const [addr, setAddr] = useState(testData);
    const [street, setStreet] = useState(testStreetData);
    const [filteredAddr, setFilteredAddr] = useState([]);
    const [filteredStreet, setFilteredStreet] = useState([]);
    const [selectedAddrValue, setSelectedAddrValue] = useState({});
    const [selectedStreetValue, setSelectedStreetValue] = useState({});
    const dataArray = [
        { title: 'Loại khách hàng', content: '1' },
        { title: 'Loại dịch vụ', content: '2' },
        { title: 'Gói cước', content: '3' },
    ];

    const _renderHeader = (item, expanded) => {
        return (
            <Form style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: 'rgba(63, 81, 181, 1)',
                borderColor: 'white',
                borderTopWidth: 5
            }}>
                <Label style={{ color: '#fff', fontWeight: 'bold' }}>
                    {" "}{item.title}
                </Label>
                {expanded
                    ? <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-up" />
                    : <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-down" />}
            </Form>
        );
    }
    const _renderContent = (item) => {
        if (item.content == '1') {
            return (
                <Form>
                    <Grid style={stylesLib.grid}>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Phòng giao dịch
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        disabled
                                        placeholder='Chọn PGD'
                                    />
                                    <Icon active name='caret-down' fontSize={15} type='FontAwesome5' />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Loại khách hàng
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        disabled
                                        placeholder='Chọn loại khách hàng'
                                    />
                                    <Icon active name='caret-down' fontSize={15} type='FontAwesome5' />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Họ tên
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập họ và tên'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    CCCD
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập CCCD'
                                        keyboardType="numeric"
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Ngày cấp
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập ngày cấp'
                                    />
                                </Item>
                            </Col>
                        </Row>

                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Tên công ty
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập tên công ty'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Mã số thuế
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập mã số thuế'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Địa chỉ lắp đặt
                                </Label>
                            </Col>
                            <Col>
                                <Item
                                    onPress={getAddress}>
                                    <Input
                                        disabled
                                        placeholder='Nhập địa chỉ'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Địa chỉ báo cước
                                </Label>
                            </Col>
                            <Col>
                                <Item
                                    onPress={getAddress}>
                                    <Input
                                        disabled
                                        placeholder='Nhập địa chỉ'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Địa chỉ hóa đơn
                                </Label>
                            </Col>
                            <Col>
                                <Item
                                    // disabled
                                    onPress={getAddress}>
                                    <Input disabled
                                        placeholder='Nhập địa chỉ'
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Số điện thoại
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập số điện thoại'
                                        keyboardType="numeric"
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Số tài khoản
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        placeholder='Nhập số tài khoản'
                                        keyboardType="numeric"
                                    />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Ngân hàng
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        disabled
                                        placeholder='Chọn ngân hàng'
                                    />
                                    <Icon active name='caret-down' fontSize={15} type='FontAwesome5' />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Loại dịch vụ
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        disabled
                                        placeholder='Chọn loại dịch vụ'
                                    />
                                    <Icon active name='caret-down' fontSize={15} type='FontAwesome5' />
                                </Item>
                            </Col>
                        </Row>
                        <Row style={[stylesLib.p_10, stylesLib.border_1]}>
                            <Col style={stylesLib.l_block_w_3}>
                                <Label
                                    style={[
                                        stylesLib.small_txt,
                                        stylesLib.b_txt]}>
                                    Gói cước
                                </Label>
                            </Col>
                            <Col>
                                <Item>
                                    <Input
                                        disabled
                                        placeholder='Chọn gói cước'
                                    />
                                    <Icon active name='caret-down' fontSize={15} type='FontAwesome5' />
                                </Item>
                            </Col>
                        </Row>
                    </Grid>
                </Form>
            );
        }
        else if (item.content == '2') {

        }
        else {

        }
    }
    const getAddress = () => {
        setModalVisible(!modalVisible);
    }
    const findAddr = (text, type) => {
        if (type == 'street') {
            if (street) {
                text = encodeURIComponent(lowerCase(text));
                setFilteredStreet(street.filter((item) =>
                    encodeURIComponent(lowerCase(item.addr)).includes(text) == true));
                console.log(filteredStreet);
            }
            else {
                setFilteredStreet([]);
            }
        }
        else {
            if (addr) {
                text = lowerCase(text);
                // text = encodeURIComponent(lowerCase(text));
                setFilteredAddr(addr.filter((item) => lowerCase(item.addr).includes(text) == true));
                // setFilteredAddr(addr.filter((item) => encodeURIComponent(lowerCase(item.addr)).includes(text) == true));
                console.log(filteredAddr);
            }
            else {
                setFilteredAddr([]);
            }
        }
    }
    return (
        <Container>
            <Content>
                <Accordion
                    dataArray={dataArray}
                    animation={true}
                    expanded={0}
                    renderHeader={_renderHeader}
                    renderContent={_renderContent}
                />
            </Content>
            <Modal
                animationType='slide'
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible);
                }}>
                <View style={modal_styles.centeredView}>
                    <View style={modal_styles.mainModalView}>
                        <TouchableOpacity
                            style={modal_styles.closeBtn}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}>
                            <Icon type="FontAwesome" name="close"></Icon>
                        </TouchableOpacity>
                        <View style={modal_styles.headerModal}>
                            <Label style={stylesLib.b_txt}>Nhập địa chỉ khách hàng</Label>
                        </View>
                        <View style={modal_styles.contentModal}>
                            <Text style={[stylesLib.mt_10, stylesLib.mb_10, stylesLib.b_txt]}>Tỉnh/Thành phố</Text>
                            <View style={modal_styles.block}>
                                <View style={[styles.autocompleteContainer, { zIndex: 3 }]}>
                                    <Autocomplete
                                        style={{
                                            backgroundColor: '#ffffff',
                                            borderWidth: 0,
                                            paddingLeft: 10,

                                        }}
                                        listContainerStyle={{
                                            padding: 10,
                                        }}
                                        autoCorrect={false}
                                        data={filteredAddr}
                                        defaultValue={JSON.stringify(selectedAddrValue) === '{}' ? '' : selectedAddrValue.addr}
                                        onChangeText={(text) => findAddr(text, 'addr')}
                                        placeholder='Tỉnh/Thành phố'
                                        flatListProps={{
                                            keyboardShouldPersistTaps: 'always',
                                            // keyExtractor: (item = testData[0]) => item.id,
                                            renderItem: ({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setSelectedAddrValue(item);
                                                        setFilteredAddr([]);
                                                    }} >
                                                    <Text>{item.addr}</Text>
                                                </TouchableOpacity>
                                            )
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={[stylesLib.mt_10, stylesLib.mb_10, stylesLib.b_txt]}>Quận/Huyện</Text>
                            <View style={modal_styles.block}>
                                <View style={[styles.autocompleteContainer, { zIndex: 3 }]}>
                                    <Autocomplete
                                        style={{
                                            backgroundColor: '#ffffff',
                                            borderWidth: 0,
                                            paddingLeft: 10,

                                        }}
                                        listContainerStyle={{
                                            padding: 10,
                                        }}
                                        autoCorrect={false}
                                        data={filteredAddr}
                                        defaultValue={JSON.stringify(selectedAddrValue) === '{}' ? '' : selectedAddrValue.addr}
                                        onChangeText={(text) => findAddr(text, 'addr')}
                                        placeholder='Quận/Huyện'
                                        flatListProps={{
                                            keyboardShouldPersistTaps: 'always',
                                            // keyExtractor: (item = testData[0]) => item.id,
                                            renderItem: ({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setSelectedAddrValue(item);
                                                        setFilteredAddr([]);
                                                    }} >
                                                    <Text>{item.addr}</Text>
                                                </TouchableOpacity>
                                            )
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={[stylesLib.mt_10, stylesLib.mb_10, stylesLib.b_txt]}>Phường/Xã</Text>
                            <View style={modal_styles.block}>
                                <View style={[styles.autocompleteContainer, { zIndex: 3 }]}>
                                    <Autocomplete
                                        style={{
                                            backgroundColor: '#ffffff',
                                            borderWidth: 0,
                                            paddingLeft: 10,

                                        }}
                                        listContainerStyle={{
                                            padding: 10,
                                        }}
                                        autoCorrect={false}
                                        data={filteredAddr}
                                        defaultValue={JSON.stringify(selectedAddrValue) === '{}' ? '' : selectedAddrValue.addr}
                                        onChangeText={(text) => findAddr(text, 'addr')}
                                        placeholder='Phường/Xã'
                                        flatListProps={{
                                            keyboardShouldPersistTaps: 'always',
                                            // keyExtractor: (item = testData[0]) => item.id,
                                            renderItem: ({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setSelectedAddrValue(item);
                                                        setFilteredAddr([]);
                                                    }} >
                                                    <Text>{item.addr}</Text>
                                                </TouchableOpacity>
                                            )
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={[stylesLib.mb_10, stylesLib.b_txt]}>Đường</Text>
                            <View style={modal_styles.block}>
                                <View style={[styles.autocompleteContainer, { zIndex: 2 }]}>
                                    <Autocomplete
                                        style={{
                                            backgroundColor: '#ffffff',
                                            borderWidth: 0,
                                            paddingLeft: 10
                                        }}
                                        listContainerStyle={{
                                            padding: 10,
                                        }}
                                        autoCorrect={false}
                                        data={filteredStreet}
                                        defaultValue={JSON.stringify(selectedStreetValue) === '{}' ? '' : selectedStreetValue.addr}
                                        onChangeText={(text) => findAddr(text, 'street')}
                                        placeholder='Nhập tên đường'
                                        flatListProps={{
                                            keyboardShouldPersistTaps: 'always',
                                            // keyExtractor: (item = testData[0]) => item.id,
                                            renderItem: ({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setSelectedStreetValue(item);
                                                        setFilteredStreet([]);
                                                    }} >
                                                    <Text>{item.addr}</Text>
                                                </TouchableOpacity>
                                            )
                                        }}
                                    />
                                </View>
                            </View>
                            <View>
                                <Text style={[stylesLib.mb_10, stylesLib.b_txt]}>Số nhà</Text>
                                <Form>
                                    <Item regular>
                                        <Input style={{ height: 30, fontSize: 12 }} />
                                    </Item>
                                </Form>
                            </View>
                        </View>
                        <View style={modal_styles.footer}>
                            <TouchableOpacity
                                style={styles.saveBtn}>
                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </Container >
    );
}

export default NewCustomer;

const styles = StyleSheet.create({
    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        backgroundColor: '#fff',
        borderRadius: 2
    },
    saveBtn: {
        backgroundColor: '#2146C7',
        margin: 20,
        padding: 10,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 5
    }
});