//React import here
import * as React from 'react';
import {
  StatusBar,
  StyleSheet,
  Alert,
  Dimensions,
  Modal,
  TouchableOpacity,
  Linking,
} from 'react-native';
//External imports here
import {
  Container,
  Content,
  Form,
  Item,
  Label,
  Input,
  Textarea,
  Spinner,
  View,
  Button,
  Thumbnail,
  Accordion,
  Text,
  Icon,
  Picker,
  Toast,
  Body,
  ListItem,
} from 'native-base';
import MapView from 'react-native-maps';
import * as Location from 'expo-location';
import { Grid, Row, Col } from 'react-native-easy-grid';

//Custom imports here
import { UserContext } from '../context/UserContext';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { ScheduleList } from '../components/ScheduleList';
import { HistoryComponent } from '../components/HistoryComponent';
import MaintenanceScreen from './MaintenanceScreen';
import { formatDateTime, getRegion } from '../utils/Utils';
import locationpng from '../assets/img/location.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CardGroupThietBi from '../components/CardGroupThietBi';
import { GroupThietBi, ThietBiRow } from '../models/GroupThietBi';
import ModalAddIndoorEquip from '../components/ModalAddIndoorEquip';
import SubCardGroupThietBi from '../components/SubCardGroupThietBi';
import MultiSelect from 'react-native-multiple-select';
import closeIcon from '../assets/img/close_icon.png';
import { get } from 'react-native/Libraries/Utilities/PixelRatio';
import ImgComponent from '../components/ImgComponent';
import EquipmentList from '../components/EquipmentComponent';

const DetailedIndoorSchedule = ({ route, navigation }) => {
  //Context
  const { user, setUser } = React.useContext(UserContext);
  //Nav params
  const { scheduleId, note } = route.params;
  //State
  const [state, setState] = React.useState({
    data: null,
    loading: true,
    groups: null
  });

  // variable for transfering calendar
  let multiSelect = null;
  const [subState, setSubState] = React.useState({
    NVNhan: undefined,
    PGD: undefined,
    selectedItems: [],
    GhiChu: ''
  })
  const [variable, setVariable] = React.useState({
    listNhanVien: [],
  });
  let listNhanVien = [];


  const [enableCheckIn, setEnableCheckIn] = React.useState(false);
  const [enableThaoTac, setEnableThaoTac] = React.useState(false);
  const [selectedThaoTac, setSelectedThaoTac] = React.useState('0');
  const [modalVisible, setModalVisible] = React.useState(false);
  const [cancelModalVisible, setCancelModalVisible] = React.useState(false);
  const [imageVisible, setImageVisible] = React.useState(false);
  const [expandImage, setExpandImage] = React.useState('');
  // const [customer, setCustomer] = React.useState(null);

  //Map
  const [margin, setMargin] = React.useState(0);
  const [region, setRegion] = React.useState({
    latitude: 10.757399253082067,
    longitude: 106.76181085807956,
    latitudeDelta: 0.0027,
    longitudeDelta: 0.0315,
  });
  const [userLocation, setUserLocation] = React.useState({
    latitude: 10.757399253082067,
    longitude: 106.76181085807956,
  });

  //Vat tu
  const [equipList, setEquipList] = React.useState({
    groups: [],
    loading: true,
    status: true
  });
  const modalAddThietBi = React.useRef(null);
  const imgRef = React.useRef(null);
  const [images, setImages] = React.useState({
    LocalImage: [],
    // multipleUrl: [],
    data: [],
  })

  const showModalAddThietBi = (index: number) => () => {
    modalAddThietBi.current.showModal(index);
  }
  const removeGroup = (index: number) => () => {
    let groups: Array<GroupThietBi> = [...equipList.groups];
    groups.splice(index, 1);
    setEquipList({ ...equipList, groups });
  }

  const addItem = (index: number, item: ThietBiRow) => {
    // console.log(item);
    let groups: Array<GroupThietBi> = [...equipList.groups];
    groups[index].ListThietBi.push(item);
    setEquipList({ ...equipList, groups });
  }
  const removeItem = (groupIdx: number) => (index: number) => () => {
    let groups: Array<GroupThietBi> = [...equipList.groups];
    groups[groupIdx].ListThietBi.splice(index, 1);
    setEquipList({ ...equipList, groups });
  }

  //API
  const loadData = () => {
    const payload: ScheduleIdPayload = {
      yc_id: scheduleId,
    };
    callAPI(
      ApiMethods.GET_DETAILED_SCHEDULE_INDOOR,
      user.Token,
      payload,
      processData,
      null
    );
  };

  const processData = (data: Array<ScheduleModel>) => {
    // console.log(data);
    if (data.length > 0) {
      data[0].yc_id = data[0].LichId;
      setState({
        ...state,
        data: data[0],
        loading: false,
      });
      if (
        data[0].CheckInTime !== null &&
        data[0].TrangThaiLich === 'XULY'
      ) {
        setEnableCheckIn(false);
        setEnableThaoTac(true);
      } else if (
        data[0].CheckInTime === null &&
        data[0].TrangThaiLich === 'XULY'
      ) {
        setEnableCheckIn(true);
        setEnableThaoTac(false);
      } else {
        setEnableCheckIn(false);
        setEnableThaoTac(false);
      }
      // getCustomerLocation(data[0].CN_ID, data[0].MaThueBao);
    } else {
      alert('Không có thông tin của lịch này');
    }
    if (data[0].ListVatTu) {
      let ListThietBi = [];
      data[0].ListVatTu.forEach((item) => {
        let data = {
          DMTB_ID: item.VatTuId,
          SO_LUONG: item.SoLuong,
          Unit: global.DsVatTuIndoor.find((element) => {
            if (element.Id === item.VatTuId.toString()) return element;
          }).Value,
          TenThietBi: global.DsVatTuIndoor.find((element) => {
            if (element.Id === item.VatTuId.toString()) return element;
          }).Text,
        };
        ListThietBi.push(data);
      })
      let groups = [{
        SO_HOPDONG: "",
        MaThueBao: data[0].TB_MATHUEBAO,
        ListThietBi: ListThietBi
      }]
      let status = new Date(Date.now()).getTime() - new Date(data[0].NgayXuLy).getTime() > 60 * 60 * 24 * 1000;
      setEquipList({
        ...equipList,
        groups: groups,
        loading: false,
        status: status
      });
    }
    else {
      let groups = [{
        SO_HOPDONG: "",
        MaThueBao: data[0].TB_MATHUEBAO,
        ListThietBi: []
      }];
      let status = new Date(Date.now()).getTime() - new Date(data[0].NgayXuLy).getTime() > 60 * 60 * 24 * 1000;
      setEquipList({
        ...equipList,
        groups: groups,
        loading: false,
        status: status
      });
    }
  };

  const handleCheckIn = async () => {
    if (status === 'denied') {
      Alert.alert('Không truy cập được vị trí', 'Vui lòng cho phép truy cập vị trí hiện tại của thiết bị để hoàn tất tác vụ.')
    }
    else {
      let location = await Location.getCurrentPositionAsync({});
      const payload: CheckInPayload = {
        yc_id: scheduleId,
        nd_id: user.nd_id,
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      };
      callAPI(ApiMethods.CHECK_IN_INDOOR, user.Token, payload, processCheckin, null);
    }
  };

  const processCheckin = () => {
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    loadData();
  };

  //Location
  // let map: MapView = null;
  const map = React.useRef(null);
  const [location, setLocation] = React.useState(null);
  const [errorMsg, setErrorMsg] = React.useState(null);
  const [status, setStatus] = React.useState('denied');
  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      setStatus(status);
      // getCurrentLocation();
      // setTimeout(() => setMargin(0), 1000);
    })();
  }, []);
  const getCurrentLocation = async () => {
    let param = await AsyncStorage.getItem(`@equip_${scheduleId}`);
    param = JSON.parse(param);
    setState({ ...state, groups: param });
    let location = await Location.getCurrentPositionAsync({});
    let _region = {
      ...region,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    map.current.animateToRegion(_region);
    // setTimeout(() => setMargin(0), 1000);
    if (margin === 0) {
      setMargin(1);
    }
    else {
      setMargin(0);
    }
  };

  const getCustomerLocation = () => {
    const payload: CustomerLocationPayload = {
      CN_ID: state.data.CN_ID,
      MaThueBao: state.data.TB_MATHUEBAO,
    };
    callAPI(
      ApiMethods.CUSTOMER_GET_LOCATION,
      user.Token,
      payload,
      processCustomerLocation,
      null
    );
  };
  const processCustomerLocation = (data: Array<Location>) => {
    if (data == null || data.length === 0) {
      Toast.show({
        text: 'Vị trí khách hàng chưa được cập nhập.',
        duration: 2000,
        position: 'bottom',
        textStyle: { textAlign: 'center' },
      });
      getCurrentLocation();
    } else {
      map.current.animateToRegion({
        latitude: data[0].Latitude,
        longitude: data[0].Longitude,
      });
    }
  };

  const [dataArray, setDataArray] = React.useState([
    { title: 'Thông tin lịch', content: '1' },
    { title: 'Thao tác', content: '2' },
  ]);

  //onLoad
  React.useEffect(() => {
    if (note !== 'personal_calendar') {
      setDataArray([
        { title: 'Thông tin lịch', content: '1' },
        { title: 'Danh sách vật tư', content: '3' },
        { title: 'Hình ảnh', content: '4' }
      ]);
    };
    loadData();
  }, []);

  const onChoose = (param) => {
    const payload: CustomerSearchPayload = {
      CN_ID: user.CN_ID,
      query: state.data.TB_MATHUEBAO,
    };
    callAPI(
      ApiMethods.CUSTOMER_SEARCH,
      user.Token,
      payload,
      param,
      null
    );
  };

  const loadCusInfo = (data) => {
    // setCustomer(data[0]);
    navigation.push('CustomerInformationScreen', { customer: data[0], schedule: state.data });
  };

  const changeModem = (data) => {
    navigation.push('ChangeDeviceScreen', { TTTB: data[0], CN_ID: state.data.CN_ID, PGD_ID: state.data.PGD_ID });
  }

  const onSubmitIndoor = () => {
    const payload = {
      LichId: state.data.LichId,
      VatTu: equipList.groups[0].ListThietBi
    };
    callAPI(
      ApiMethods.UPDATE_VATTU_INDOOR,
      user.Token,
      payload,
      processSubmitIndoor,
      errorSubmit
    );
  }
  const processSubmit = (data) => {
    global.refreshIndoorHome();
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.pop();
  };
  const processSubmitIndoor = (data) => {
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.pop();
  };
  const errorSubmit = (error) => {
    Toast.show({
      text: 'Cập nhật thất bại' + error.message,
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
  };
  // cancel calendar
  const onCancel = () => {
    const payload = {
      LichId: state.data.LichId,
    };
    callAPI(
      ApiMethods.HUY_LICH_INDOOR,
      user.Token,
      payload,
      processSubmit,
      errorSubmit
    );
  }

  // transfer calendar
  React.useEffect(() => {
    resetState();
  }, []);
  const resetState = () => {
    setModalVisible(false);
    setSubState({ ...subState, PGD: global.listLich[0].DanhMucPGD[0].Id });
    listNhanVien = global.listLich[0].NhanVienTheoLich.filter((data) => {
      return (
        data.Reference ===
        global.listLich[0].DanhMucPGD.find((element) => {
          if (element.Id === global.listLich[0].DanhMucPGD[0].Id)
            return element;
        }).Id
      );
    });
    setVariable({ ...variable, listNhanVien: listNhanVien });
  };

  const onValueChange_PGD = (value: string) => {
    setSubState({ ...subState, PGD: value });
    listNhanVien = global.listLich[0].NhanVienTheoLich.filter((data) => {
      return (
        data.Reference ===
        global.listLich[0].DanhMucPGD.find((element) => {
          if (element.Id === value) return element;
        }).Id
      );
    });
    setVariable({ ...variable, listNhanVien: listNhanVien });
  };

  const onSelectedItemsChange = (selectedItems) => {
    setSubState({ ...subState, selectedItems: selectedItems });
  };

  const onTransfer = () => {
    if (subState.selectedItems.length == 0) {
      Alert.alert('Thông báo', 'Vui lòng chọn nhân viên');
    } else {
      const payload = {
        LichId: state.data.LichId,
        NhanVienId: parseInt(subState.selectedItems[0]),
        GhiChu: subState.GhiChu
      };
      // Alert.alert(JSON.stringify(payload));
      callAPI(ApiMethods.CHUYEN_LICH_INDOOR, user.Token, payload, processSubmit, errorSubmit);
    }
  };

  const onExpand = (item) => {
    setImageVisible(!imageVisible);
    setExpandImage(item);
  }

  const addImage = (images) => {
    setImages(images);
  }

  React.useEffect(() => {
    console.log(images);
  }, [images]);

  const _renderHeader = (item, expanded) => {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: 'rgba(63, 81, 181, 1)',
        borderColor: 'white',
        borderTopWidth: 5
      }}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>
          {" "}{item.title}
        </Text>
        {expanded
          ? <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-up" />
          : <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-down" />}
      </View>
    );
  }
  const _renderContent = (item) => {
    if (item.content === '1') {
      return (
        <Form>
          <Item fixedLabel disabled>
            <Label style={styles.title}>Mã thuê bao:</Label>
            <Input disabled value={state.data.TB_MATHUEBAO} />
          </Item>
          <Item fixedLabel disabled>
            <Label style={styles.title}>Trạng thái:</Label>
            <Input disabled value={state.data.TrangThaiLich} />
          </Item>
          <Item fixedLabel disabled>
            <Label style={styles.title}>Họ tên:</Label>
            <Input disabled value={state.data.TenLienHe} />
          </Item>
          <TouchableOpacity
            style={{ justifyContent: 'flex-start' }}
            onPress={() => Linking.openURL(`tel:${state.data.DienThoai}`)}>
            <Item fixedLabel disabled>
              <Label style={styles.title}>Điện thoại:</Label>
              <Input disabled value={state.data.DienThoai} />
              <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-call`} />
            </Item>
          </TouchableOpacity>
          <Item stackedLabel disabled>
            <Label style={styles.title}>Địa chỉ:</Label>
            <Textarea disabled value={state.data.DiaChi} rowSpan={2} />
          </Item>
          {/* <Item fixedLabel disabled>
            <Label style={styles.title}>Gói cước:</Label>
            <Input disabled value='260000' />
          </Item>
          <Item fixedLabel disabled>
            <Label style={styles.title}>Tốc độ gói cước:</Label>
            <Input disabled value='40Mb' />
          </Item> */}
          <Item fixedLabel disabled>
            <Label style={styles.title}>Node:</Label>
            <Input disabled value={state.data.Node} />
          </Item>
          {
            state.data.TrangThaiLich === 'HOANTAT' ? (
              <Item stackedLabel disabled>
                <Label style={styles.title}>Nội dung xử lý:</Label>
                <Textarea disabled value={state.data.NoiDungXuLy} />
              </Item>
            ) : null
          }
          <Item stackedLabel disabled>
            <Label style={styles.title}>Ghi chú:</Label>
            <Textarea disabled value={state.data.GhiChu} style={{ margin: 8 }} />
          </Item>
          {note === 'personal_calendar' ? (
            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
              <Button warning full
                style={{ margin: 10 }}
                onPress={() => { setModalVisible(true) }}>
                <Text>Chuyển lịch</Text>
              </Button>
              <Button danger
                style={{ margin: 10 }}
                onPress={() => { setCancelModalVisible(true) }}>
                <Text>Hủy lịch</Text>
              </Button>
            </View>
          ) : null}
          {state.data.TB_MATHUEBAO != '' &&
            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
              <Button full
                onPress={() => onChoose(loadCusInfo)}
                style={{ marginBottom: 20, marginTop: 5, borderRadius: 2, padding: 20 }}>
                <Text>Thông tin khách hàng</Text>
              </Button>
            </View>
          }
        </Form>
      );
    } else if (item.content === '2') {
      return (
        <View>
          <View>
            <MapView
              style={[styles.mapStyle, { margin: margin }]}
              initialRegion={region}
              showsUserLocation={true}
              showsMyLocationButton={true}
              onMapReady={getCustomerLocation}
              ref={map}
            />
            {/* <Button
              square
              transparent
              style={{
                position: 'absolute', top: 5, right: 9, opacity: 0.8
              }}
              onPress={getCurrentLocation}>
              <Thumbnail
                square
                small
                source={locationpng}
                style={{ opacity: 0.5 }}
              />
            </Button> */}
          </View>
          <View>
            {enableCheckIn ? (
              <Button
                full
                rounded
                style={{ margin: 30, marginTop: 10 }}
                onPress={handleCheckIn}
                disabled={!enableCheckIn}>
                <Text>Check In</Text>
              </Button>) : null
            }
            {enableThaoTac ? (
              <Form>
                <Button warning full
                  style={{ margin: 30, marginTop: 10 }}
                  onPress={() => navigation.navigate('SuppliesList', { schedule: state.data, groups: state.groups, type: 'indoor', status: 'BT' })}>
                  <Text>Hoàn tất</Text>
                </Button>
                {/* <Item Picker>
                  <Picker
                    mode="dropdown"
                    enabled={enableThaoTac}
                    iosIcon={<Icon name="arrow-down" />}
                    selectedValue={selectedThaoTac}
                    onValueChange={onValueChange}>
                    <Picker.Item label="Thao tác" value="0" />
                    <Picker.Item label="Hoàn tất" value="1" />
                    <Picker.Item
                      label="Xử lý"
                      value="PostponeScheduleModal"
                    />
                    <Picker.Item label="Hủy lịch" value="CancelScheduleModal" />
                  </Picker>
                </Item> */}
              </Form>) : null}
          </View>
        </View>
      );
    }
    else if (item.content === '3') {
      return equipList.loading ? (
        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <Text style={{ fontStyle: 'italic', fontWeight: 'bold' }}>Không có danh sách vật tư.</Text>
        </View>
      ) : (
        <View>
          {/* <ImgComponent
            ref = {(el) => imgRef.current = el}
            images = {images}
            addImage={addImage}>
          </ImgComponent>
          <EquipmentList>
            
          </EquipmentList> */}
          {!equipList.status && equipList.groups.map((item, index) => (
            <CardGroupThietBi group={item} key={index}
              showModal={showModalAddThietBi(index)}
              removeGroup={removeGroup(index)}
              removeItem={removeItem(index)}
            />
          ))}
          {equipList.status && equipList.groups.map((item, index) => (
            <SubCardGroupThietBi group={item} key={index} />
          ))}
          <Button
            disabled={equipList.status}
            style={{ padding: 10, margin: 10, alignSelf: 'center' }}
            onPress={onSubmitIndoor}>
            <Text>Lưu</Text>
          </Button>
        </View>)
    }
    else if (item.content === '4') {
      return !state.data.ImgUrls || (state.data.ImgUrls && state.data.ImgUrls.length) < 1 ? (
        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <Text style={{ fontStyle: 'italic', fontWeight: 'bold' }}>Không có hình ảnh.</Text>
        </View>
      ) : (
        <View>
          <Grid>
            <Row>
              {state.data.ImgUrls.map((item, index) => (
                <Col key={index}>
                  <ListItem>
                    <TouchableOpacity style={{ padding: 5 }}
                      onPress={() => onExpand(item)}
                    >
                      {/* <Icon type="FontAwesome" name="expand" /> */}
                      <Thumbnail
                        square
                        large
                        key={index}
                        source={{ uri: global.baseUrl + item }}
                        style={{ height: 100, width: 100 }}
                      />
                    </TouchableOpacity>
                  </ListItem>
                </Col>
              ))}
            </Row>
          </Grid>
        </View>
      )
    };
  };

  return state.loading ? (
    <Spinner />
  ) : (
    <Container>
      <Content padder style={{ backgroundColor: 'white' }}>
        <Accordion
          dataArray={dataArray}
          animation={true}
          expanded={0}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
        />
      </Content>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <Container>
          <View style={styles.centeredView}>
            <Content>
              <TouchableOpacity
                style={styles.closeBtn}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Thumbnail
                  square
                  source={closeIcon}
                  style={{ width: 30, height: 30 }}
                />
              </TouchableOpacity>
              <View style={[styles.modalView, { width: Dimensions.get('window').width }]}>
                <View style={styles.headerModal}>
                  <Text style={styles.modalText}>Chuyển lịch</Text>
                </View>
                <View>
                  <Grid>
                    <Row style={styles.row}>
                      <Col style={styles.leftBlock}>
                        <Text style={styles.textStyle}>Phòng giao dịch:</Text>
                      </Col>
                      <Col style={styles.rightBlock}>
                        <Form>
                          <Item Picker style={{ marginLeft: 0 }}>
                            <Picker
                              mode="dropdown"
                              iosIcon={<Icon name="arrow-down" />}
                              selectedValue={subState.PGD}
                              onValueChange={onValueChange_PGD}>
                              {global.listLich[0].DanhMucPGD.map((item, index) => {
                                return (
                                  <Picker.Item
                                    label={item.Text}
                                    value={item.Id}
                                    key={index}
                                  />
                                );
                              })}
                            </Picker>
                          </Item>
                        </Form>
                      </Col>
                    </Row>
                    <Row style={styles.row}>
                      <Col style={styles.leftBlock}>
                        <Text style={styles.textStyle}>Nhân viên nhận:</Text>
                      </Col>
                      <Col style={styles.rightBlock}>
                        <MultiSelect
                          hideTags
                          single={true}
                          fontSize={15}
                          items={
                            variable.listNhanVien.length > 0
                              ? variable.listNhanVien
                              : []
                          }
                          uniqueKey="Id"
                          ref={(component) => {
                            multiSelect = component;
                          }}
                          onSelectedItemsChange={onSelectedItemsChange}
                          selectedItems={subState.selectedItems}
                          selectText="Chọn nhân viên"
                          searchInputPlaceholderText="Tìm kiếm nhân viên..."
                          // altFontFamily="ProximaNova-Light"
                          tagRemoveIconColor="#CCC"
                          tagBorderColor="#CCC"
                          tagTextColor="#CCC"
                          textColor="#000"
                          selectedItemTextColor="#CCC"
                          selectedItemIconColor="#CCC"
                          itemTextColor="#000"
                          displayKey="Text"
                          searchInputStyle={{ color: '#000' }}></MultiSelect>
                      </Col>
                    </Row>
                    <Row style={styles.row}>
                      <Col style={styles.leftBlock}>
                        <Text style={styles.textStyle}>Ghi chú:</Text>
                      </Col>
                      <Col style={styles.rightBlock}>
                        <Textarea
                          style={styles.noteBlock}
                          value={subState.GhiChu}
                          placeholder="Nội dung ghi chú"
                          rowSpan={2}
                          onChangeText={(text) => {
                            setSubState({ ...subState, GhiChu: text });
                          }}
                        />
                      </Col>
                    </Row>
                  </Grid>
                  <Button rounded onPress={onTransfer} style={{ margin: 10, alignSelf: 'center', padding: 10 }}>
                    <Text>Chuyển lịch</Text>
                  </Button>
                </View>
              </View>
            </Content>
          </View>
        </Container>
      </Modal>
      <Modal
        animationType="fade"
        transparent={true}
        visible={imageVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <Container>
          <View>
            <TouchableOpacity
              style={styles.closeBtn}
              onPress={() => {
                setImageVisible(!imageVisible);
              }}>
              <Thumbnail
                // square
                source={closeIcon}
                style={{ width: 30, height: 30 }}
              />
            </TouchableOpacity>
            <Thumbnail
              large
              square
              style={{
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
              }}
              source={{ uri: global.baseUrl + expandImage }} />
          </View>
        </Container>
      </Modal>
      <Modal
        animationType="fade"
        transparent={true}
        visible={cancelModalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={[styles.centeredView, { alignItems: 'center', justifyContent: 'center' }]}>
          <View style={[styles.modalView, { width: Dimensions.get('window').width * 0.8, }]}>
            <View style={styles.headerModal}>
              <Text style={styles.modalText}>Xác nhận hủy lịch</Text>
            </View>
            <View style={styles.footerModal}>
              <Button
                danger
                onPress={() => {
                  setCancelModalVisible(!cancelModalVisible);
                }}
                style={{ margin: 10, padding: 10 }}>
                <Text>Không</Text>
              </Button>
              <Button
                success
                onPress={onCancel}
                style={{ margin: 10, padding: 10 }}>
                <Text>Có</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
      <ModalAddIndoorEquip ref={(el) => modalAddThietBi.current = el} addItem={addItem} />
    </Container>
  );
};

const styles = StyleSheet.create({
  mapStyle: {
    width: Dimensions.get('window').width * 0.95,
    height: Dimensions.get('window').width,
  },
  headerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderBottomWidth: 0.5,
    // width: Dimensions.get('window').width,
  },
  modalText: {
    fontWeight: 'bold',
  },
  centeredView: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 10
  },
  openButton: {
    margin: 10,
    padding: 10

  },
  modalView: {
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 5,
  },
  title: {
    color: 'rgb(0,0,0)',
    fontWeight: 'bold'
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.4,
    justifyContent: 'center',
    padding: 16
  },
  rightBlock: {
    width: Dimensions.get('window').width * 0.5,
    justifyContent: 'center',
    margin: 16
  },
  row: {
    borderBottomWidth: 0.5,
  },
  closeBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 5,
  },
  textStyle: {
    paddingLeft: 5,
    fontWeight: 'bold',
    fontSize: 14
  },
  noteBlock: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  footerModal: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10
  }
});
export default DetailedIndoorSchedule;
