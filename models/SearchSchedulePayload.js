export type SearchSchedulePayload = {
  nd_theolich_id: number,
  TuNgay: Date,
  DenNgay: Date,
};
